# Arm Manipulation
This repositoty contains the packages to perform the manipulation of objects for the RoboCup@Home competition.

<img src="images/demo.gif" width="1000">

## How to get help
You can contact me at hugomirandaqueiros@gmail.com for any help related to the code or the installation of this repository.

## Hardware used
To perform the manipulation a [**xArm6**](https://www.ufactory.cc/product-page/ufactory-xarm-6) arm is used along a custom gripper controlled with a [**Dynamixel XM430-W210-R**](https://emanual.robotis.com/docs/en/dxl/x/xm430-w210/) motor. Additionally, the object detection uses a [**ZED Mini**](https://www.stereolabs.com/docs/get-started-with-zed/) camera.

## Content
* The package that performs the object detection with the ZED Mini camera and the [Detectron2](https://github.com/facebookresearch/detectron2) algorithm:
    * [camera_control](../camera_control)
* The package that controls the gripper with the Dynamixel motor:
    * [gripper_control](../gripper_control)
* The package that contains all the interfaces for inter-node communication used in this project:
    * [xarm_interfaces](../xarm_interfaces)
* The package that centralizes all the manipulation actions and that contains the main launch file to start the manipulation:
    * [xarm_main_control](../xarm_main_control)
* A folder with all the packages used to control the xArm6 arm:
    * [xarm_ros2](https://gitlab.epfl.ch/create-lab/robocup_at_home/arm_manipulation/xarm_ros2.git)
* A folder containing the scripts to build and run the base Docker image used for this project:
    * [docker_base](../docker_base)
* A folder with all the html documentation files automatically generated with Doxygen, and a link to the report written for this project (refer to the section [Documentation](#documentation) for more details about how to generate them):
    * [docs](../docs)
> **Note**:
> * This repository contains a submodule [xarm_ros2](https://gitlab.epfl.ch/create-lab/robocup_at_home/arm_manipulation/xarm_ros2.git) that also contains another submodule [cxx](https://gitlab.epfl.ch/create-lab/robocup_at_home/arm_manipulation/cxx.git). Both are forks from the original xArm Github repos [xarm_ros2](https://github.com/xArm-Developer/xarm_ros2.git) and [cxx](https://github.com/xArm-Developer/xArm-CPLUS-SDK.git). The reason these repos were forked was to allow changing their content for the RoboCup while keeping them up to date with their original sources.
> * In order to setup and to update these submodules automatically two shell scripts are provided in this repo, namely `setup_submodules.sh` and `update_submodules.sh`. <br />
Please refer to the [Submodules management](#submodules-managment) section to see how to use them.

## Requirements
* These packages were only tested with [`ROS 2 Humble`](https://docs.ros.org/en/ros2_documentation/humble/) on `Ubuntu 22.04 LTS`. It is hence highly advised to use this setup.
* You also need to have `Python >= 3.10` and `C++ >= 17` on your computer in order to run everything.
* Finally you need to have a computer with a `Nvidia GPU` in order to use the ZED Mini camera.

## Setup Instructions
* Create a workspace:
    ```bash
    # Skip this step if you already have a target workspace
    $ cd ~
    $ mkdir -p dev_ws/src
    ```
* Clone the repo:
    ```bash
    $ cd ~/dev_ws/src
    $ git clone git@gitlab.epfl.ch:create-lab/robocup_at_home/arm_manipulation/arm_manipulation.git
    ```
* Setup the submodules:
    ```bash
    $ cd ~/dev_ws/src/arm_manipulation
    $ chmod +x setup_submodules.sh
    $ ./setup_submodules.sh
    ```
* Update the submodules:
    ```bash
    $ cd ~/dev_ws/src/arm_manipulation
    $ chmod +x update_submodules.sh
    $ ./update_submodules.sh
    ```
* Download the Detectron2 weights
    ```bash
    $ cd ~/dev_ws/src
    $ mkdir resource
    $ cd resource
    $ wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1z9I7aZmqr9qiw2fKSLJYLcbKJiMXylN_' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1z9I7aZmqr9qiw2fKSLJYLcbKJiMXylN_" -O model_best_train_output_batch4_epoch50_lr0.001_202303310127.pth && rm -rf /tmp/cookies.txt
    ```
* ⚠️ Allow the Dynamixel to be controlled by the user by using the port `/dev/ttyUSB0`:
    ```bash
    # Reboot your computer after executing this command in order to apply the changes
    $ sudo usermod -aG dialout $USER
    ```

## Setup the real xArm6
If you want to use the real xArm6, you need to connect it by ethernet to your computer. For that you need to configure the network as showed in the following image: <br />

![](images/network_configuration.png)

In order to test the connection, you can ping the xArm6 by running the following command:
```bash
$ ping 192.168.1.234
```
If you get a response, it means that the connection is working.

## Setup the hardware installation
If you want to perform a real manipulation task with the xArm6, you need to setup the hardware installation as showed in the following image: <br />

![](images/hardware_installation.png)

## Build and Run Instructions
If you want to run the project in your `host machine`, please refer to the [HOST.md](HOST.md) file and follow all the instructions. <br />

If you want to run the project in a `Docker container` to avoid the burden of installing everything from scratch, please refer to the [DOCKER.md](DOCKER.md) file and follow all the instructions.
    
## Different commands to test the xArm6
> **Note**: Run the next commands in different terminals than the one you used to build the workspace and don't forget to source the ROS workspace in each of them (sourcing is done automatically in the Docker container). <br />
> When using the real xArm6, you need to make sure that it is connected to the computer by pinging it.

### Simulated robot MoveIt control with Rviz GUI
```bash
$ ros2 launch xarm_moveit_config xarm6_moveit_fake.launch.py
```

### Simulated robot MoveIt control with code
```bash
# Open terminal 1
$ ros2 launch xarm_planner xarm6_planner_fake.launch.py

# Open terminal 2
$ ros2 launch xarm_planner test_xarm_planner_api_joint.launch.py dof:=6 robot_type:=xarm
```

### Real robot MoveIt control with Rviz GUI
```bash
$ ros2 launch xarm_moveit_config xarm6_moveit_realmove.launch.py robot_ip:=192.168.1.234
```

### Real robot MoveIt control with code
```bash
# Open terminal 1
$ ros2 launch xarm_planner xarm6_planner_realmove.launch.py robot_ip:=192.168.1.234

# Open terminal 2
$ ros2 launch xarm_planner test_xarm_planner_api_joint.launch.py dof:=6 robot_type:=xarm
```

## Documentation
In order to automatically generate the documentation for this repository, you need to install [Doxygen](https://www.doxygen.nl/index.html), you can do it by running:
```bash
$ sudo apt-get -y install doxygen
```
Then you can generate the documentation with Doxygen by running the `generate_docs.sh` script:
```bash
$ cd ~/dev_ws/src/arm_manipulation
$ chmod +x generate_docs.sh
$ ./generate_docs.sh
```
All the generated documentation html files will be in the `docs` folder at the root level of this repository.

## Submodules management
This repository depends on two submodules: [xarm_ros2](https://gitlab.epfl.ch/create-lab/robocup_at_home/arm_manipulation/xarm_ros2.git) that also contains another submodule [cxx](https://gitlab.epfl.ch/create-lab/robocup_at_home/arm_manipulation/cxx.git). Both are forks from the original xArm Github repos [xarm_ros2](https://github.com/xArm-Developer/xarm_ros2.git) and [cxx](https://github.com/xArm-Developer/xArm-CPLUS-SDK.git). <br />

If you followed correctly the setup instructions, you should already have the submodules setup and updated thanks to the `setup_submodules.sh` and `update_submodules.sh` scripts. <br />

Each submodule contains two remotes: `origin` and `upstream`. The `origin` remote is the one that points to the repository on Gitlab. The `upstream` remote is the one that points to the original repository on Github. <br />

By running the `update_submodules.sh` script, it will update the custom submodules branches to the latest commit from the `origin` remote first. Then, it will also update the submodules to the latest commit from the `upstream` remote, on the remote `origin` branches `humble` for [xarm_ros2](https://gitlab.epfl.ch/create-lab/robocup_at_home/arm_manipulation/xarm_ros2.git) and on the branch `master` for [cxx](https://gitlab.epfl.ch/create-lab/robocup_at_home/arm_manipulation/cxx.git). <br />

Like that you can work independently on these submodules by using the `origin` remote on the branch `custom_humble` for [xarm_ros2](https://gitlab.epfl.ch/create-lab/robocup_at_home/arm_manipulation/xarm_ros2.git) and on the branch `custom` for [cxx](https://gitlab.epfl.ch/create-lab/robocup_at_home/arm_manipulation/cxx.git). Then, you can merge manually the `upstream` remote tracking branches into your custom branches whenever you want to update the submodules. <br />

⚠️ **Important**: Most of the time, when changing branches or pulling changes, submodules will be updated correctly automatically thanks to the config variables setup in the `setup_submodules.sh`. However in some edge cases it may need manual intervention, in this case running the `update_submodules.sh` will ensure that everything is up to date and solve any synchronization problem. <br />

**Running the `update_submodules.sh` by default every time you change branches or pull changes is hence highly advised to avoid any unwanted edge case update errors.**

