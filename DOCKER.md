# Build and Run Instructions when using the Docker container
If you want to run the project in a `Docker container` to avoid the burden of installing everything from scratch, please follow all the instructions in this file. <br />
The Docker image used for this project is the [`hmirandaqueiros/arm_manipulation:full`](https://hub.docker.com/r/hmirandaqueiros/arm_manipulation). Be aware that the final Docker container will weight more than `30GB`.

## Prerequisites
* Install [CUDA Toolkit](https://developer.nvidia.com/cuda-11-7-0-download-archive?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=22.04&target_type=deb_network) (Version 11.7 is the most suited for this project)
> **Note**:
>
> Before installing the `Nvidia Container Toolkit`, you need to install the correct `Nvidia` drivers in your host machine. You can do that automatically by installing the `CUDA Toolkit` version 11.7 mentioned above. <br />
> Be aware that you need to specify the exact version you want to install, like in this example : `sudo apt-get install cuda-11-7`, otherwise you will end up always with the latest available version. <br />
> If you still have some problems with the drivers after the install, delete first all the `Nvidia` drivers and toolkits from your host machine by typing the following commands and then install the `CUDA Toolkit` from scratch again:
> ```bash
> $ sudo apt-get purge nvidia*
> $ sudo apt-get --purge remove "*cublas*" "cuda*" "nsight*" 
> $ sudo rm -rf /usr/local/cuda* 
> $ sudo apt-get autoremove
> $ sudo apt-get autoclean
> $ sudo apt-get clean
>```
> After a succesful installation of the `CUDA Toolkit`, you can check if the drivers are installed correctly by typing the following commands:
> ```bash
> # This command should display the version of your CUDA Toolkit
> $ nvcc --version
> # This command should display the drivers of your GPU
> $ nvidia-smi
> ```
> If those commands are not recognized, you need to add the `CUDA Toolkit` to your `.bashrc` file by adding the following lines at the end of the file:
> ```bash
> # Nvidia CUDA Toolkit
> export CUDA_HOME=/usr/local/cuda-11.7
> export LD_LIBRARY_PATH=${CUDA_HOME}/lib64:$LD_LIBRARY_PATH
> export PATH=${CUDA_HOME}/bin:${PATH}
> ```

* Install [Docker Engine](https://docs.docker.com/engine/install/ubuntu)
* Install [Nvidia Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker) in order to use your `Nvidia` GPU with Docker.
* Install [Dynamixel Wizard 2](https://emanual.robotis.com/docs/en/software/dynamixel/dynamixel_wizard2/#install-linux) (Optional) to control the Dynamixel with a GUI
> **Note**:
> Make sure to follow all the prerequisites at the previous links in order to install everything correctly.

## Launch and Build Instructions for the Docker container
After installing all the previous requirements, you can launch the Docker container by running the following command in your host machine:
```bash
# Execute the run.sh script in your host machine that will pull the image from Docker Hub and run it
$ cd ~/dev_ws/src/arm_manipulation
$ chmod +x run.sh
$ ./run.sh
```
> **Note**:
> > When the container starts it connects your `arm_manipulation` directory to the container's one so that you can develop your code in your host machine and run it in the container. <br />
Everytime you run the container, it starts by building all the packages in your workspace. If you notice that the build at the start of the container is taking too much time, you may want to build again the Docker image by running the `build.sh` script in order to update your latest changes to the image:
> ```bash
> # Execute the build.sh script in your host machine that will build again the image
> $ cd ~/dev_ws/src/arm_manipulation
> $ chmod +x build.sh
> $ ./build.sh
> ```

## Build package changes
If you want to build all the workspace manually again, you can run the following command inside the container terminal:
```bash
$ cd ~/dev_ws
$ sudo colcon build 
```

If you want to build only a specific package, you can run the following command inside the container terminal:
```bash
$ cd ~/dev_ws
$ sudo colcon build --packages-select <package_name>
```

## Run Instructions for the Docker container
The next instructions will allow you to run the demo of the project which is a pick and place task where the xArm6 picks and places 2 broccolis in a plate and 1 bottle in a box. <br />

In the figure below, you can see an overview of the strategy/behavior used to perform this pick and place task:

![Strategy](images/strategy.png)

Moreover, you can find at the following links two videos of the demo: <br /> 
One at [normal speed](https://drive.google.com/file/d/1QJ_u9ZKnZEqKociS9sD8Yd3_Ekj40o7t/view?usp=sharing) and another one at [3x speed](https://drive.google.com/file/d/1rWNe8_0isAHpHqZDJiqU45TFgElhecSA/view?usp=sharing). <br />

In order to run the project in the Docker container, you need to follow the next steps:

* Open one additional terminal in the container by running the following command in your host machine in a new terminal:
    ```bash
    # Execute the following command in as many terminals as you want in your host machine in order to open several terminals in the container
    $ sudo docker exec -it arm_manipulation_full bash
    ```
    > **Note**: To close the container terminal, just type `exit` in the terminal or press `Ctrl + D`.
* Launch the project:
    > **Note**: Run the next commands in the two container's terminals you opened in the previous steps. However be careful to never run these commands in the terminals you used to build manually the ROS workspace otherwise you will get source errors.

    ```bash
    # Launch Rviz and MoveIt 2 in a first terminal
    $ ros2 launch xarm_planner xarm6_planner_realmove.launch.py robot_ip:=192.168.1.234

    # Launch the project in a second terminal
    $ ros2 launch xarm_main_control xarm_main_control.launch.py dof:=6 robot_type:=xarm
    ```
    > **Note**: Before running the previous command, you need to make sure that the Dynamixel is connected to the computer and that the port `/dev/ttyUSB0` is available. In particular, make sure that you ran the last command of the [Setup Instructions](#setup-instructions) section. <br />
    Moreover, the default Dynamixel ID is 10, so make sure that the Dynamixel you are using has this ID too, otherwise you can change it by using the Dynamixel Wizard. You can check all of that by running the following command in the container and look if there are any error messages:
    > ```bash
    > $ ros2 run gripper_control gripper_control
    > ```

    > You also need to make sure that the ZED Mini camera is connected correctly to the computer. You can check that by running the following command in the container and look if there are any error messages:
    > ```bash
    > $ ros2 run camera_control camera_control
    > ```

    > Finally, you need to ensure that the xArm6 is connected correctly to the computer by pinging it.