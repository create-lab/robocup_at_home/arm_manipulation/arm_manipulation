# Docker Base
This folder contains the Dockerfile and the scripts to build and run the Docker base image [`hmirandaqueiros/arm_manipulation:base`](https://hub.docker.com/r/hmirandaqueiros/arm_manipulation) for the `Arm Manipulation` project. <br />
This image shloud be rebuilt only if there is a need to update the base image by changing the `Dockerfile`.

## Build Instructions
In order to build the Docker image, you need to run the following command in your host machine:
```bash
# Execute the build.sh script in your host machine that will build the image
$ cd ~/dev_ws/src/arm_manipulation/docker_base
$ chmod +x build.sh
$ ./build.sh
```

## Run Instructions
In order to run the Docker image, you need to run the following command in your host machine:
```bash
# Execute the run.sh script in your host machine that will run the image
$ cd ~/dev_ws/src/arm_manipulation/docker_base
$ chmod +x run.sh
$ ./run.sh
```


