from setuptools import setup

package_name = 'camera_control'
object_detection = 'camera_control/object_detection'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name, object_detection],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='hugom',
    maintainer_email='hugomirandaqueiros@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'camera_control = camera_control.camera_control:main',
        ],
    },
)
