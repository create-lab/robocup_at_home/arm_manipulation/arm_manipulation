import cv2
import rclpy
import numpy as np

from detectron2.config import get_cfg
from detectron2.engine.defaults import DefaultPredictor
from detectron2.data import MetadataCatalog

from ..camera import Camera
from .predictor import Predictor


class ObjectDetector:
    """A class used to instantiate the object detection algorithm (here Detectron2) and allows getting
    the pose of the object in the camera frame and the angle by how much the gripper should rotate to pick it.

    Parameters
    ----------
    camera : Camera
        The camera object used to get the image and the 3D coordinates of the object.

    Attributes
    ----------
    camera : Camera
        The camera object used to get the image and the 3D coordinates of the object.
    logger : rclpy.logging.Logger
        The logger used to log messages.
    cfg : detectron2.config.CfgNode
        The configuration of the model.
    class_names : list
        The list of the names of the objects that the model can detect.
    predictor : Predictor
        The predictor used to run the model on an image.
    """

    def __init__(self, camera: Camera) -> None:
        self.camera = camera
        self.logger = rclpy.logging.get_logger("object_detector")

        model_file_path = "./src/arm_manipulation/camera_control/configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"
        model_weight = "./src/ressource/model_best_train_output_batch4_epoch50_lr0.001_202303310127.pth"

        self.cfg = get_cfg()
        self.cfg.merge_from_file(model_file_path)
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.8
        self.cfg.MODEL.WEIGHTS = model_weight
        self.cfg.MODEL.DEVICE = "cuda"

        self.class_names = MetadataCatalog.get("valid").thing_classes = [
            "bottle",
            "plate",  # bowl
            "box",
            "broccoli",
            "carrot",
            "cookie",
            "plate",  # cup
            "egg",
            "food_container",
            "green_bean",
            "meatball",
            "orange_juice",
            "plate",
            "sausage",
            "spaghetti",
            "tray",
        ]

        self.cfg.MODEL.ROI_HEADS.NUM_CLASSES = 16
        self.cfg.DATASETS.TEST = ("valid",)

        self.cfg.freeze()

        self.predictor = Predictor(self.cfg, parallel=False)

    def detect_object(self, image: np.ndarray, object_name: str) -> (np.ndarray, dict):
        """Detects the object in the image and returns the image with the object annotated and a
        dictionary containing the pose of the object and the angle by how much the gripper should rotate to pick it.

        Parameters
        ----------
        image : np.ndarray
            The image in which the object is detected.
        object_name : str
            The name of the object to detect.

        Returns
        -------
        result_image : np.ndarray
            The image with the object annotated.
        result_dict : dict
            A dictionary containing the pose of the object and the angle by how much the gripper should rotate to pick it.

        When several objects of the same type are detected, only the closest one to the center of
        the camera is returned.
        """
        result_dict = {}

        predictions, result_image = self.predictor.run_on_image(image)
        predictions = predictions["instances"]

        boxes = predictions.pred_boxes if predictions.has("pred_boxes") else None
        class_ids = (
            predictions.pred_classes.cpu().numpy()
            if predictions.has("pred_classes")
            else None
        )
        masks = (
            predictions.pred_masks.cpu().numpy()
            if predictions.has("pred_masks")
            else None
        )
        pred_class_names = list(map(lambda x: self.class_names[x], class_ids))
        pred_class_names = np.array(pred_class_names)

        if pred_class_names.size == 0:
            return result_image, result_dict

        object_idxs = np.where(pred_class_names == object_name)[0]
        object_idx = None
        # only get the object that is the closest to the center of the camera
        if object_idxs.size > 1:
            centers = []
            for idx in object_idxs:
                x = int(boxes[int(idx)].get_centers()[0][0])
                y = int(boxes[int(idx)].get_centers()[0][1])
                centers.append((x, y))

            # sort the centers in a list by increasing distance towards the middle of the image
            distance = np.sqrt(
                (self.camera.xm - np.array(centers)[:, 0]) ** 2
                + (self.camera.ym - np.array(centers)[:, 1]) ** 2
            )
            sorted_idxs = np.argsort(distance)

            # Get the center of the closest object
            object_idx = int(object_idxs[sorted_idxs[0]])

        if object_idxs.size == 1:
            object_idx = int(object_idxs[0])

        if object_idx is not None:
            centers = boxes[object_idx].get_centers()[0]
            xb = int(centers[0])
            yb = int(centers[1])
            xc, yc, zc = self.camera.get_point3D(xb, yb)

            result_dict[pred_class_names[object_idx]] = {}
            result_dict[pred_class_names[object_idx]]["center"] = (
                xc,
                yc,
                zc,
            )

            object_mask = masks[object_idx]
            result_dict[pred_class_names[object_idx]][
                "gripper_angle"
            ] = self.get_gripper_angle(object_mask)

            self.annotate_image(result_image, xb, yb, zc)

        return result_image, result_dict

    def annotate_image(self, image: np.ndarray, xb: int, yb: int, zc: float) -> None:
        """Annotates the image with a circle at the center of the object and the depth value at the
        center of the object.

        Parameters
        ----------
        image : np.ndarray
            The image to annotate.
        xb : int
            The x coordinate of the center of the object.
        yb : int
            The y coordinate of the center of the object.
        zc : float
            The depth value at the center of the object in meters.
        """
        # Draw a circle at the center of the object
        cv2.circle(
            image,
            (xb, yb),
            radius=0,
            color=(255, 0, 0),
            thickness=10,
        )

        # Display the depth value at the center of the object
        cv2.putText(
            image,
            "Depth=" + str(round(zc, 3)),
            (xb - 60, yb - 15),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.6,
            (0, 0, 0),
            2,
            cv2.LINE_AA,
        )

    def get_gripper_angle(self, object_mask: np.ndarray) -> float:
        """Get the angle in degrees by how much the gripper should rotate to pick the object, based on the mask of the object

        Parameters
        ----------
        object_mask : np.ndarray
            The mask of the object.

        Returns
        -------
        gripper_angle : float
            The angle in degrees by how much the gripper should rotate to pick it.
        """
        # convert the mask to a binary image
        object_mask = object_mask.astype(np.uint8)

        # fit a rectangle to the mask
        contours, _ = cv2.findContours(
            object_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
        )
        rect = cv2.minAreaRect(contours[0])
        (_, _), (width, height), rotation = rect

        gripper_angle = self.translateRotation(rotation, width, height)

        return gripper_angle

    def translateRotation(self, rotation: float, width: float, height: float) -> float:
        """Translate the rotation of the rectangle fitting the object to the gripper angle

        Parameters
        ----------
        rotation : float
            The rotation in degrees of the rectangle fitting the object.
        width : float
            The width of the rectangle fitting the object.
        height : float
            The height of the rectangle fitting the object.

        Returns
        -------
        gripper_angle : float
            The angle in degrees by how much the gripper should rotate to pick the object.
        """
        if width < height:
            rotation = 180 - rotation
        else:
            rotation = 90 - rotation
        return rotation
