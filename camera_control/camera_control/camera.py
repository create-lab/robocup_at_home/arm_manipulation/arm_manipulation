import rclpy
import pyzed.sl as sl
import numpy as np
import math


class Camera:
    """A class to control the ZED Mini camera.

    Attributes
    ----------
    logger : rclpy.logging.Logger
        The logger to display messages.
    zed : pyzed.sl.Camera
        The ZED camera object.
    image_size : pyzed.sl.Resolution
        The size of the image.
    image_zed : pyzed.sl.Mat
        The image retrieved from the camera.
    depth_map : pyzed.sl.Mat
        The depth map retrieved from the camera.
    point_cloud : pyzed.sl.Mat
        The point cloud retrieved from the camera.
    xm : int
        The x coordinate of the middle of the image.
    ym : int
        The y coordinate of the middle of the image.
    search_pairs : list
        The list of pairs of coordinates to search for depth around the point of interest when there is no depth value availble at the point of interest.

    """

    def __init__(self) -> None:
        self.logger = rclpy.logging.get_logger("camera")
        self.setup_camera()

        # get the coordinates of the middle of the image
        self.xm = self.image_size.width // 2
        self.ym = self.image_size.height // 2

        # get the pair of coordinates of the points to search for depth
        search_list = [x for x in range(-600, 600)]
        self.search_pairs = [
            (a, b) for idx, a in enumerate(search_list) for b in search_list[idx + 1 :]
        ]
        self.search_pairs = sorted(
            self.search_pairs, key=lambda x: x[0] ** 2 + x[1] ** 2
        )

    def setup_camera(self) -> None:
        """Sets up the camera."""
        # Create a ZED camera object
        self.zed = sl.Camera()

        # Set configuration parameters
        init_params = sl.InitParameters()
        init_params.camera_resolution = sl.RESOLUTION.HD720
        init_params.depth_mode = (
            sl.DEPTH_MODE.ULTRA
        )  # Use ULTRA depth mode for high accuracy
        init_params.coordinate_units = (
            sl.UNIT.METER
        )  # Use meter units (for depth measurements)
        # Sets the minimum depth perception distance to 0.05 instead of 0.1 for the ZED mini
        init_params.depth_minimum_distance = 0.05

        # Open the camera
        err = self.zed.open(init_params)
        if err != sl.ERROR_CODE.SUCCESS:
            self.logger.error("Camera access failed")
            exit(-1)

        # Create and set RuntimeParameters after opening the camera
        runtime_params = sl.RuntimeParameters()
        runtime_params.sensing_mode = (
            sl.SENSING_MODE.STANDARD
        )  # Use STANDARD sensing mode
        # Setting the depth confidence parameters [0; 100] : points closer to 100 are not reliable
        runtime_params.confidence_threshold = 90
        runtime_params.textureness_confidence_threshold = 100

        self.image_size = self.zed.get_camera_information().camera_resolution
        self.logger.info(
            "image size: "
            + str(self.image_size.width)
            + ", "
            + str(self.image_size.height)
        )

        self.image_zed = sl.Mat()
        self.depth_map = sl.Mat()
        self.point_cloud = sl.Mat()

    def get_image(self) -> np.ndarray:
        """Gets the image from the camera.

        Returns
        -------
        image : np.ndarray
            The image from the camera.
        """
        if self.zed.grab() == sl.ERROR_CODE.SUCCESS:
            # Retrieve the left image (the one close to the ZED Mini logo)
            self.zed.retrieve_image(self.image_zed, sl.VIEW.LEFT)

            # Use get_data() to get the BGR image numpy array
            image = self.image_zed.get_data()[:, :, :3]  # remove alpha channel
            image = np.ascontiguousarray(
                image, dtype=np.uint8
            )  # convert to contiguous array for opencCV
            return image
        else:
            raise Exception("Image access failed")

    def get_depth(self, x: int, y: int) -> float:
        """Gets the depth value at the specified coordinates of the image.

        Parameters
        ----------
        x : int
            The x coordinate of the point of interest in the image.
        y : int
            The y coordinate of the point of interest in the image.

        Returns
        -------
        depth : float
            The depth value in meters at the specified coordinates of the image.

        Raises
        ------
        Exception
            If the depth access failed.

        """
        if self.zed.grab() == sl.ERROR_CODE.SUCCESS:
            # Retrieve the depth map aligned on the left eye of the camera (the one close to the ZED Mini logo)
            self.zed.retrieve_measure(self.depth_map, sl.MEASURE.DEPTH)

            i = 0
            xd = 0
            yd = 0

            # Get the depth value for pixel (x, y)
            depth_array = self.depth_map.get_value(x, y)
            error_code = depth_array[0]
            depth = depth_array[1]
            while error_code != sl.ERROR_CODE.SUCCESS or self.is_nan(depth):
                (xd, yd) = self.search_pairs[i]
                i += 1

                if i >= len(self.search_pairs):
                    raise Exception("Depth access failed after trying all pairs")

                while (
                    x + xd < 0
                    or x + xd >= self.image_size.width
                    or y + yd < 0
                    or y + yd >= self.image_size.height
                ):
                    (xd, yd) = self.search_pairs[i]
                    i += 1

                    if i >= len(self.search_pairs):
                        raise Exception("Depth access failed after trying all pairs")

                depth_array = self.depth_map.get_value(x + xd, y + yd)
                error_code = depth_array[0]
                depth = depth_array[1]

            return depth
        else:
            raise Exception("Depth access failed")

    def get_point3D(self, x: int, y: int) -> (float, float, float):
        """Gets the 3D point value at the specified coordinates of the image.

        Parameters
        ----------
        x : int
            The x coordinate of the point of interest in the image.
        y : int
            The y coordinate of the point of interest in the image.

        Returns
        -------
        xf : float
            The x coordinate in meters of the 3D point.
        yf : float
            The y coordinate in meters of the 3D point.
        zf : float
            The z coordinate in meters of the 3D point.

        Raises
        ------
        Exception
            If the point cloud access failed.
        """
        if self.zed.grab() == sl.ERROR_CODE.SUCCESS:
            # Retrieve the point cloud aligned on the left eye of the camera (the one close to the ZED Mini logo)
            self.zed.retrieve_measure(self.point_cloud, sl.MEASURE.XYZBGRA)

            i = 0
            xd = 0
            yd = 0

            # Get the 3D point value for pixel (x, y)
            point3D = self.point_cloud.get_value(x, y)
            error_code = point3D[0]
            xf = point3D[1][0]
            yf = point3D[1][1]
            zf = point3D[1][2]
            while (
                error_code != sl.ERROR_CODE.SUCCESS
                or self.is_nan(xf)
                or self.is_nan(yf)
                or self.is_nan(zf)
            ):
                (xd, yd) = self.search_pairs[i]
                i += 1

                if i >= len(self.search_pairs):
                    raise Exception("Point cloud access failed after trying all pairs")

                while (
                    x + xd < 0
                    or x + xd >= self.image_size.width
                    or y + yd < 0
                    or y + yd >= self.image_size.height
                ):
                    (xd, yd) = self.search_pairs[i]
                    i += 1

                    if i >= len(self.search_pairs):
                        raise Exception(
                            "Point cloud access failed after trying all pairs"
                        )

                point3D = self.point_cloud.get_value(x + xd, y + yd)
                error_code = point3D[0]
                xf = point3D[1][0]
                yf = point3D[1][1]
                zf = point3D[1][2]

            return xf, yf, zf
        else:
            raise Exception("Point cloud access failed")

    def is_nan(self, x: float) -> bool:
        """Checks if the value is NaN.

        Parameters
        ----------
        x : float
            The value to check.

        Returns
        -------
        bool
            True if the value is NaN, False otherwise.

        """
        return (
            x == float("inf")
            or x == float("-inf")
            or math.isnan(x)
            or (math.isinf(x) and x > 0)
            or (math.isinf(x) and x < 0)
            or np.isinf(x)
            or np.isnan(x)
        )
