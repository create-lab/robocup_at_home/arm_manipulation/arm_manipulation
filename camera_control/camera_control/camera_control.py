import rclpy
from rclpy.node import Node
from .object_detection.object_detector import ObjectDetector
from .camera import Camera

import cv2

from std_msgs.msg import Bool
from std_msgs.msg import String
from geometry_msgs.msg import TransformStamped
from xarm_interfaces.srv import GetObjectPose
from xarm_interfaces.srv import GetGripperAngle


class CameraControl(Node):
    """A class that instantiates a ROS 2 node to control the camera and detect objects in the image.

    Attributes
    ----------
    Node : rclpy.node.Node
        The ROS 2 node.
    camera : Camera
        The camera object used to get the image and the 3D coordinates of the object.
    object_detector : ObjectDetector
        The object detector object used to detect the object in the image.
    object_name : str
        The name of the object to detect.
    object_name_subscriber : rclpy.subscription.Subscription
        The subscriber to the object_name topic.
    get_object_pose_service : rclpy.service.Service
        The service to get the pose of the object.
    get_gripper_angle_service : rclpy.service.Service
        The service to get the angle in degrees by how much the gripper should rotate to pick the object.
    result_dict : dict
        A dictionary containing the pose of the object and the gripper angle to pick it.
    timer : rclpy.timer.Timer
        The timer to stream the camera.
    """

    def __init__(self) -> None:
        super().__init__("camera_control")
        # creates the camera
        self.camera = Camera()

        # creates object detector
        self.object_detector = ObjectDetector(self.camera)

        self.object_name = "none"
        # creates object_name subscriber
        self.object_name_subscriber = self.create_subscription(
            String, "object_name", self.object_name_callback, 10
        )

        # creates get_object_pose_service
        self.get_object_pose_service = self.create_service(
            GetObjectPose, "get_object_pose", self.get_object_pose_callback
        )

        # creates get_gripper_angle service
        self.get_gripper_angle_service = self.create_service(
            GetGripperAngle, "get_gripper_angle", self.get_gripper_angle_callback
        )

        self.result_dict = {}

        timer_period = 0.01  # seconds
        self.timer = self.create_timer(timer_period, self.stream_camera)

        self.get_logger().info("camera_control is ready")

    def stream_camera(self) -> None:
        """Streams the camera and detects the object in the image."""
        image = None
        try:
            image = self.camera.get_image()
        except Exception as e:
            self.get_logger().error(str(e))
            return 0

        result_image = image
        if self.object_name != "none":
            result_image, self.result_dict = self.object_detector.detect_object(
                image, self.object_name
            )
        else:
            self.result_dict = {}

        # Draw a red circle at the center of the image
        cv2.circle(
            result_image,
            (self.camera.xm, self.camera.ym),
            radius=0,
            color=(0, 0, 255),
            thickness=10,
        )

        cv2.imshow("ZED", result_image)
        cv2.waitKey(5)

    def object_name_callback(self, msg: String) -> None:
        """Callback function for the object_name subscriber.

        Parameters
        ----------
        msg : std_msgs.msg.String
            The message containing the object name.

        When the object name is received, it is stored in the object_name attribute.
        """
        self.object_name = msg.data
        self.get_logger().info("received object name: " + self.object_name)

    def get_object_pose_callback(
        self, request: GetObjectPose.Request, response: GetObjectPose.Response
    ) -> GetObjectPose.Response:
        """Callback function for the get_object_pose service.

        Parameters
        ----------
        request : GetObjectPose.Request
            The request containing the object name.
        response : GetObjectPose.Response
            The response containing the pose of the object in the camera frame and a boolean to specify if the object is found.

        Returns
        -------
        response : GetObjectPose.Response
            The response containing the pose of the object in the camera frame and a boolean to specify if the object is found.
        """
        object_name = request.object_name

        t = TransformStamped()
        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = "camera"
        t.child_frame_id = object_name

        # no rotation for the object (it is just seen as a point)
        t.transform.rotation.x = 0.0
        t.transform.rotation.y = 0.0
        t.transform.rotation.z = 0.0
        t.transform.rotation.w = 1.0

        if object_name in self.result_dict:
            (x, y, z) = self.result_dict[object_name]["center"]
            response.is_object_found = True
            # object coordinates
            t.transform.translation.x = x
            t.transform.translation.y = y
            t.transform.translation.z = z
            response.object_transform = t

        else:
            response.is_object_found = False
            response.object_transform = t

        return response

    def get_gripper_angle_callback(
        self, request: GetGripperAngle.Request, response: GetGripperAngle.Response
    ) -> GetGripperAngle.Response:
        """Callback function for the get_gripper_angle service.

        Parameters
        ----------
        request : GetGripperAngle.Request
            The request containing the object name.
        response : GetGripperAngle.Response
            The response containing the angle in degrees by how much the gripper should rotate to pick the object and a boolean to specify if the object is found.

        Returns
        -------
        response : GetGripperAngle.Response
            The response containing the angle in degrees by how much the gripper should rotate to pick the object and a boolean to specify if the object is found.
        """
        object_name = request.object_name

        if object_name in self.result_dict:
            gripper_angle = self.result_dict[object_name]["gripper_angle"]
            response.is_object_found = True
            response.gripper_angle = float(gripper_angle)

        else:
            response.is_object_found = False
            response.gripper_angle = 0.0

        return response


def main(args: list = None) -> None:
    """Main function of the camera_control file that initializes and starts spinning the ROS 2 node.

    Parameters
    ----------
    args : list, optional
        The arguments to pass to the ROS 2 node.
    """
    rclpy.init(args=args)

    camera_control = CameraControl()

    rclpy.spin(camera_control)

    cv2.destroyAllWindows()
    self.camera.zed.close()
    camera_control.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
