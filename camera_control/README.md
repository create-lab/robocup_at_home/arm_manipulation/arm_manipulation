# Camera Control
This package performs the object detection with the ZED Mini camera and the [Detectron2](https://github.com/facebookresearch/detectron2) algorithm.

## Content
This package is composed of one node defined in the file `camera_control.py`. 

* **camera_control.py**:
  > This file contains the node used to instantiate the camera and the object detection algorithm. It also creates a subscriber to the topic `object_name` in order to get the name of the object to detect. Additionnally, it creates two services `get_object_pose` and `get_gripper_angle` that are used by the `xarm_main_control` package to get the pose of the object and the angle by how much the gripper should rotate to pick it.
* **camera.py**:
  > This file contains the class `Camera` that is used to instantiate the camera (here the ZED Mini) and allows getting all the mesurements from it.
* **object_detector.py**:
  > This file contains the class `ObjectDetector` that is used to instantiate the object detection algorithm (here Detectron2) and allows getting the pose of the object in the camera frame and the angle by how much the gripper should rotate to pick it.
* **predictor.py**: 
  > This file contains the class `Predictor` that is used to instantiate the predictor of the object detection algorithm (here Detectron2). It was taken from the Detectron2 source build and modified to fit our needs.

## Build Instructions
If you want to build this package, you can use the following command:
```bash
$ colcon build --packages-select camera_control
```

## Run Instructions
**Note**: Be sure to follow all the requirements, preparation and setup instructions sections of the main [README](../README.md) file before running this package.

To run this package, you can use the following command:
```bash
$ ros2 run camera_control camera_control
```
> If you see a camera connection error make sure that you connected the camera correctly and that you installed the whole project correctly.