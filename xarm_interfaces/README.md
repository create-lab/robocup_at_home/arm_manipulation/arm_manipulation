# xArm Interfaces
This package contains the interfaces for inter-node communication used in the `Arm Manipulation` project.

## Content
* **srv**:
  > This `srv` folder contains the definition of the services used by the `Arm Manipulation` project:
    > * **GetGripperAngle.srv**: {`string` object_name} -> {`bool` is_object_found, `float32` gripper_angle}
    >   > This file contains the definition of the service `get_gripper_angle` that is used to get the angle in degrees by how much the gripper should rotate to pick the object.
    > * **GetObjectPose.srv**: {`string` object_name} -> {`bool` is_object_found, `geometry_msgs/TransformStamped` object_transform}
    >   > This file contains the definition of the service `get_object_pose` that is used to get the pose of the object to pick.
    > * **SetGripperOrder.srv**: {`string` order, `int32` position} -> {`bool` success}
    >   > This file contains the definition of the service `set_gripper_order` that is used to control the gripper.
    > * **GetLoad.srv**: {`uint8` id} -> {`int32` load}
    >   > This file contains the definition of the service `get_load` that is used to get the current load of the gripper.
    > * **SetTargetTf.srv**: {`geometry_msgs/TransformStamped` target} -> {`bool` success}
    >   > This file contains the definition of the service `set_target_tf` that is used to set the new target pose of the TCP of the robot. This allows computing the new end effector target pose of the robot in the file `xarm_target_tf_broadcaster.cpp`.

