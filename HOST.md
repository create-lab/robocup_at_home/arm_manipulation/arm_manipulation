# Build and Run Instructions when using the host machine
If you want to run the project in your `host machine`, please follow all the instructions in this file.

## Prerequisites
* Install [ROS 2 Humble](https://docs.ros.org/en/ros2_documentation/humble/Installation/Ubuntu-Install-Debians.html)
* Install [MoveIt 2](https://moveit.ros.org/install-moveit2/binary/) (Do not forget to install `ros-humble-rmw-cyclonedds-cpp` as specified in the link)
* Install [CUDA Toolkit](https://developer.nvidia.com/cuda-11-7-0-download-archive?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=22.04&target_type=deb_network) (Version 11.7 is the most suited for this project)
> **Note**:
> 
> Before installing the `ZED SDK` and `Detectron2`, you need to install `CUDA` and the correct `Nvidia` drivers in your host machine. You can do that automatically by installing the `CUDA Toolkit` version 11.7 mentioned above. <br />
> Be aware that you need to specify the exact version you want to install, like in this example : `sudo apt-get install cuda-11-7`, otherwise you will end up always with the latest available version. <br />
> If you still have some problems with the drivers after the install, delete first all the `Nvidia` drivers and toolkits from your host machine by typing the following commands and then install the `CUDA Toolkit` from scratch again:
> ```bash
> $ sudo apt-get purge nvidia*
> $ sudo apt-get --purge remove "*cublas*" "cuda*" "nsight*" 
> $ sudo rm -rf /usr/local/cuda* 
> $ sudo apt-get autoremove
> $ sudo apt-get autoclean
> $ sudo apt-get clean
>```
> After a succesful installation of the `CUDA Toolkit`, you can check if the drivers are installed correctly by typing the following commands:
> ```bash
> # This command should display the version of your CUDA Toolkit
> $ nvcc --version
> # This command should display the drivers of your GPU
> $ nvidia-smi
> ```
> If those commands are not recognized, you need to add the `CUDA Toolkit` to your `.bashrc` file by adding the following lines at the end of the file:
> ```bash
> # Nvidia CUDA Toolkit
> export CUDA_HOME=/usr/local/cuda-11.7
> export LD_LIBRARY_PATH=${CUDA_HOME}/lib64:$LD_LIBRARY_PATH
> export PATH=${CUDA_HOME}/bin:${PATH}
> ``` 

* Install [ZED SDK](https://www.stereolabs.com/docs/installation/linux) 3.8.2 for the ZED Mini camera
* Install [Detectron2](https://detectron2.readthedocs.io/en/latest/tutorials/install.html) for the object detection (Install [Pytorch](https://pytorch.org/) for CUDA 11.7 first)
* Install [Gazebo](https://classic.gazebosim.org/tutorials?tut=install_ubuntu) (Optional) 
* Install [gazebo_ros_pkgs](http://gazebosim.org/tutorials?tut=ros2_installing&cat=connect_ros) (Optional)
* Install [Dynamixel Wizard 2](https://emanual.robotis.com/docs/en/software/dynamixel/dynamixel_wizard2/#install-linux) (Optional) to control the Dynamixel with a GUI
> **Note**:
> * Make sure to follow all the prerequisites at the previous links in order to install everything correctly.
> * After installing all the previous requirements, your `.bashrc` file should contain all the following lines (if not add it because it ensures that MoveIt 2 and ROS 2 are sourced correctly):
> ```bash
> # Source ROS 2
> source /opt/ros/humble/setup.bash
> source /usr/share/colcon_cd/function/colcon_cd.sh
> export _colcon_cd_root=/opt/ros/humble/
> source /usr/share/colcon_argcomplete/hook/colcon-argcomplete.bash
>
> # Fix given by MoveIt 2
> export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp
>
> # Nvidia CUDA Toolkit
> export CUDA_HOME=/usr/local/cuda-11.7
> export LD_LIBRARY_PATH=${CUDA_HOME}/lib64:$LD_LIBRARY_PATH
> export PATH=${CUDA_HOME}/bin:${PATH}
>  ```

## Build Instructions
After installing all the previous prerequisites, you can build the workspace by following the next steps:
* Install the dependencies:
    ```bash
    # Remember to source ROS 2 and MoveIt 2 environment settings first if you haven't already added it to your .bashrc file
    $ cd ~/dev_ws
    & rosdep update
    $ rosdep install -i --from-path src --rosdistro humble -y
    ```
* Build the workspace:
    ```bash
    $ cd ~/dev_ws
    # Build all packages
    $ colcon build
    ```

## Build package changes
If you want to build only a specific package, you can run the following command:
```bash
$ colcon build --packages-select <package_name>
```

## Run Instructions
The next instructions will allow you to run the demo of the project which is a pick and place task where the xArm6 picks and places 2 broccolis in a plate and 1 bottle in a box. <br />

In the figure below, you can see an overview of the strategy/behavior used to perform this pick and place task:

![Strategy](images/strategy.png)

Moreover, you can find at the following links two videos of the demo: <br /> 
One at [normal speed](https://drive.google.com/file/d/1QJ_u9ZKnZEqKociS9sD8Yd3_Ekj40o7t/view?usp=sharing) and another one at [3x speed](https://drive.google.com/file/d/1rWNe8_0isAHpHqZDJiqU45TFgElhecSA/view?usp=sharing). <br />

* Source the workspace:
    ```bash
    $ cd ~/dev_ws
    $ source install/setup.bash
    ```
    > **Note**: To avoid sourcing the workspace every time you open a new terminal, you can add the following line to your `.bashrc` file:
    > ```bash
    > source ~/dev_ws/install/setup.bash
    >```

* Launch the project:
    > **Note**: Run the next commands in different terminals than the one you used to build the workspace and don't forget to source the workspace in each of them, otherwise you will get source errors.

    ```bash
    # Launch Rviz and MoveIt 2 in a first terminal
    $ ros2 launch xarm_planner xarm6_planner_realmove.launch.py robot_ip:=192.168.1.234

    # Launch the project in a second terminal
    $ ros2 launch xarm_main_control xarm_main_control.launch.py dof:=6 robot_type:=xarm
    ```
    > **Note**: Before running the previous command, you need to make sure that the Dynamixel is connected to the computer and that the port `/dev/ttyUSB0` is available. In particular, make sure that you ran the last command of the [Setup Instructions](#setup-instructions) section. <br />
    Moreover, the default Dynamixel ID is 10, so make sure that the Dynamixel you are using has this ID too, otherwise you can change it by using the Dynamixel Wizard. You can check all of that by running the following command and look if there are any error messages:
    > ```bash
    > $ ros2 run gripper_control gripper_control
    > ```

    > You also need to make sure that the ZED Mini camera is connected correctly to the computer. You can check that by running the following command and look if there are any error messages:
    > ```bash
    > $ ros2 run camera_control camera_control
    > ```

    > Finally, you need to ensure that the xArm6 is connected correctly to the computer by pinging it.