# Ensures that all the basic commands like pull, checkout, etc. take into account all the submodules recursively
git config submodule.recurse true
# Ensures that at every push, the submodules changes are pushed as well
git config push.recurseSubmodules on-demand
# Updates the submodules URL changes
git submodule sync --recursive
# Updates the submodules to the latest commit recursively by applying the merge method
git submodule update --recursive --init --remote --merge
# The following commands switch the detached HEAD to the corresponding branch and then add the remote upstream
cd ./xarm_ros2
git checkout custom_humble
git remote add upstream https://github.com/xArm-Developer/xarm_ros2.git
cd ./xarm_sdk/cxx
git checkout custom
git remote add upstream https://github.com/xArm-Developer/xArm-CPLUS-SDK.git
cd ../../..
