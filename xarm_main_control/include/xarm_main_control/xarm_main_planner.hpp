#ifndef __XARM_MAIN_PLANNER_HPP__
#define __XARM_MAIN_PLANNER_HPP__

#include "xarm_main_control/xarm_client.hpp"

#define _USE_MATH_DEFINES
#define DEGREE_TO_RADIAN M_PI / 180

/// Height of the table with respect to the base of the robot in meters.
extern const float z_table_ = -0.045;

namespace xarm_main_control
{
    /**
     * @brief A class that instantiates a ROS 2 node to plan and execute the trajectories of the xArm.
     * This class relies on the package "xarm_ros2/xarm_planner" that uses MoveIt 2.
     *
     */
    class XarmMainPlanner
    {
    private:
        rclcpp::Node::SharedPtr node_;
        xarm_planner::XArmPlanner planner_;
        xarm_main_control::XarmClient xarm_client_;
        std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
        std::unique_ptr<tf2_ros::Buffer> tf_buffer_;

    public:
        XarmMainPlanner();
        XarmMainPlanner(const rclcpp::Node::SharedPtr& node);

    public:
        geometry_msgs::msg::Pose get_robot_pose();
        std::vector<float> get_robot_joints();
        bool execute_joint_target(std::vector<float> joint_target);
        bool execute_delta_target(float dx, float dy, float dz);
        bool execute_rotation_target(float xx, float yy, float zz, float a);
        bool rotate_end_effector(float angle);
        bool rotate_base(float angle);
        bool execute_sequence_step(std::vector<float> sequence_step);

    private:
        tf2::Vector3 convert_delta_local_to_base_frame(float dx, float dy, float dz);
        tf2::Quaternion create_quaternion_from_axis_angle(float xx, float yy, float zz, float a);
    };
}
#endif // __XARM_MAIN_PLANNER_HPP__