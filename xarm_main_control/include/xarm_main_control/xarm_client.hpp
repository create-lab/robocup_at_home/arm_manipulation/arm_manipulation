#ifndef __XARM_CLIENT_HPP__
#define __XARM_CLIENT_HPP__

#include "xarm_planner/xarm_planner.h"
#include "gripper_control/gripper_client.hpp"
#include "xarm_msgs/msg/robot_msg.hpp"
#include "xarm_interfaces/srv/get_object_pose.hpp"
#include "xarm_interfaces/srv/set_gripper_order.hpp"
#include "xarm_interfaces/srv/get_gripper_angle.hpp"
#include "xarm_interfaces/srv/set_target_tf.hpp"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/buffer.h"
#include "tf2/exceptions.h"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include <functional>
#include <memory>
#include <chrono>
#include <cstdlib>
#include <tuple>

using namespace std::chrono_literals;

namespace xarm_main_control
{
    /**
     * @brief A class that instantiates a ROS 2 node that provides clients to the services used to control the xArm,
     * the gripper and get all the interesting poses.
     * 
     */
    class XarmClient
    {
    private:
        rclcpp::Node::SharedPtr node_;
        gripper_control::GripperClient gripper_client_;
        std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
        std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
        rclcpp::Client<xarm_interfaces::srv::GetObjectPose>::SharedPtr get_object_pose_client_;
        rclcpp::Client<xarm_interfaces::srv::SetGripperOrder>::SharedPtr set_gripper_order_client_;
        rclcpp::Client<xarm_interfaces::srv::GetGripperAngle>::SharedPtr get_gripper_angle_client_;
        rclcpp::Client<xarm_interfaces::srv::SetTargetTf>::SharedPtr set_target_tf_client_;

    public:
        XarmClient(const std::string &client_name);

    public:
        std::tuple<bool, geometry_msgs::msg::Pose> get_object_pose(std::string object_name);
        bool set_gripper_order(std::string order, int position = 2100);
        std::tuple<bool, float> get_gripper_angle(std::string object_name);
        bool set_target_tf(geometry_msgs::msg::TransformStamped target_tf);
        bool is_gripper_empty();
    };
}
#endif // __XARM_CLIENT_HPP__