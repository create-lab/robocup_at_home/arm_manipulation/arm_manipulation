#ifndef __XARM_MAIN_CONTROL_HPP__
#define __XARM_MAIN_CONTROL_HPP__

#include <vector>
#include <map>
#include <string>

/// @brief Gripper search opening
extern const int search_opening_ = -4500;
/// @brief Gripper release opening
extern const int release_opening_ = -2900;
/// @brief Gripper pick openings
extern const std::map<std::string, int> pick_opening_{
    {"bottle", -2400},
    {"broccoli", -3800},
    {"carrot", -3800},
};

/// @brief The order of containers to search.
extern const std::vector<std::string> container_order_{
    "box",
    "plate",
};

/// @brief The order of objects to pick.
extern const std::vector<std::string> object_order_{
    // "carrot",
    "broccoli",
    "bottle",
};

/// @brief The quantity of objects to pick.
extern const std::map<std::string, int> object_quantity_{
    {"bottle", 1},
    {"broccoli", 2},
    {"carrot", 1},
};

/// @brief The containers where to put the objects.
extern const std::map<std::string, std::string> object_container_{
    {"bottle", "box"},
    {"broccoli", "plate"},
    {"carrot", "plate"},
};

/// @brief The orientation of the objects to pick.
extern const std::map<std::string, std::string> object_orientation_{
    {"bottle", "vertical"},
    {"broccoli", "horizontal"},
    {"carrot", "horizontal"},
};

/// @brief The joint target to rest idle.
extern const std::vector<float> rest_joint_target_{-1.569334, -0.403594, 0.042084, 0.000012, 0.379106, -0.000032};
/// @brief The joint target to start searching for specific objects.
extern const std::map<std::string, std::vector<float>> search_object_joint_target_{
    {"container", {-0.817786, 0.023394, -1.003672, -0.004736, 0.976164, 0.754382}},
    {"horizontal", {-1.569375, -0.413798, -0.569862, -0.000062, 0.977866, 0.000197}},
    {"vertical", {-1.569462, 1.094097, -1.180008, 0.000261, -1.490507, 0.000059}},
};

/// @brief The sequence of movements to search for the objects.
extern const std::map<std::string, std::vector<std::vector<float>>> search_object_sequence_{
    // Search containers sequence
    {
        "container",
        {
            {0.08, 0, 0},
            {0.08, 0, 0},
            {0.08, 0, 0},
        },
    },

    // Search vertical object sequence
    {
        "vertical",
        {
            {0, -0.05, 0},
            {0, 0.15, 0},
            {0, -0.10, 0},
        },
    },

    // Search horizontal object sequence
    {
        "horizontal",
        {
            {0, -0.10, 0},
            {0.20, 0, 0},
            {0, 0.15, 0},
            {0, 0.15, 0},
            {0, 0.15, 0},
            {-0.20, 0, 0},
            {0, -0.15, 0},
            {0, -0.20, 0},
        },
    },
};

/// @brief The robot joint pose where the containers were found.
std::map<std::string, std::vector<float>> container_pose_{
    {"box", {0, 0, 0, 0, 0, 0}},
    {"plate", {0, 0, 0, 0, 0, 0}},
};

#endif // __XARM_MAIN_CONTROL_HPP__