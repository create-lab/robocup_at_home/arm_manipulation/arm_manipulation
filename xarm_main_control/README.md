# xArm Main Control
This package centralizes all the manipulation actions and contains the main launch file to start the project.

## Content
This package is composed of 5 nodes defined respectively in the files `xarm_main_control.cpp`, `xarm_main_planner.cpp`, `xarm_client.cpp`, and `xarm_target_tf_broadcaster.cpp`.

* **xarm_main_control.cpp**:
  > This file contains the main node used to control and plan the xArm manipulation. It creates a publisher for the topic `object_name` in order to send the name of the object to detect.
* **xarm_main_control.hpp**:
  > This header file contains several constants used in the `xarm_main_control.cpp` file that can be changed in order to adapt the manipulation task to your needs. 
* **xarm_main_planner.cpp**:
  > This file contains the node used to compute the trajectories of the xArm. It relies on the use of `Moveit2` methods defined in the `xarm_ros2/xarm_planner` package.
* **xarm_client.cpp**:
  > This file contains the node used to create the clients of the services `get_object_pose`, `set_gripper_order`, `get_gripper_angle` and `set_target_tf` in order to querry those services effitiently in this package.
* **xarm_target_tf_broadcaster.cpp**:
  > This file contains the node used to create the service `set_target_tf` that is used to set the new target pose of the TCP of the robot. This allows computing the new end effector target pose of the robot.
* **xarm_main_control.launch.py**:
  > This file contains the main launch file of the project. It launches all the following nodes: `xarm_main_control`, `xarm_target_tf_broadcaster`, `camera_control`, `gripper_control` and `gripper_service`. It also creates three static `tf2` transform publishers for the `arm_base`, `camera` and `tcp` frames. <br />
  In the figure below, you can see all the `tf2` frames used in the project from different points of view, when all the joints of the robot are at 0 radian:

  ![Frames](../images/frames.png)

## Build Instructions
If you want to build this package, you can use the following command:
```bash
$ colcon build --packages-select xarm_main_control
```

## Run Instructions
This package is not meant to be run alone. So if you want to run this package, follow the Run Instructions from the `Host.md` file or the `Docker.md` file.

## Utils
If you want to change the default speed and acceleration of the xArm6 robot, you can directly change the `max_velocity` and `max_acceleration` values of each joint in the `joint_limits.yaml` file located in the `xarm_ros2/xarm_moveit_config/config/xarm6` folder. <br />
With `Moveit2`, for the `planCartesianPath` you can only change the speed by modifying the `joint_limits.yaml` file mentioned above. However for the `planJointTarget`, `planPoseTarget` and `planPoseTargets` you can reduce dynamically the default speed and acceleration values of the `joint_limits.yaml` by modifying the `max_velocity_scaling_factor` and `max_acceleration_scaling_factor` values of the `xarm_planner.cpp` file located in the `xarm_ros2/xarm_planner/src` folder.

