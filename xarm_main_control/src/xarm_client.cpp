#include "xarm_main_control/xarm_client.hpp"

namespace xarm_main_control
{
    /**
     * @brief Construct a new Xarm Client:: Xarm Client object
     * 
     * @param client_name The name of the node that is instantiated.
     */
    XarmClient::XarmClient(const std::string &client_name) : gripper_client_("xarm_gripper_client")
    {
        node_ = rclcpp::Node::make_shared(client_name);
        get_object_pose_client_ = node_->create_client<xarm_interfaces::srv::GetObjectPose>("get_object_pose");
        set_gripper_order_client_ = node_->create_client<xarm_interfaces::srv::SetGripperOrder>("set_gripper_order");
        get_gripper_angle_client_ = node_->create_client<xarm_interfaces::srv::GetGripperAngle>("get_gripper_angle");
        set_target_tf_client_ = node_->create_client<xarm_interfaces::srv::SetTargetTf>("set_target_tf");
        tf_buffer_ = std::make_unique<tf2_ros::Buffer>(node_->get_clock());
        tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

        // while (!get_object_pose_client_->wait_for_service(1s) || !set_gripper_order_client_->wait_for_service(1s) ||
        //        !get_gripper_angle_client_->wait_for_service(1s) || !set_target_tf_client_->wait_for_service(1s))
        // {
        //     RCLCPP_INFO(node_->get_logger(), "service(s) not available, waiting again...");
        // }

        RCLCPP_INFO(node_->get_logger(), "%s is ready", client_name.c_str());
    }

    /**
     * @brief Get the object pose in the tcp frame.
     * 
     * @note Objects that are already in the container zone are ignored. The container zone may change depending on the
     * the setup so don't forget to perform the corresponding changes in this method.
     * 
     * @param object_name The name of the object to find.
     * @return std::tuple<bool, geometry_msgs::msg::Pose> A tuple containing a boolean that indicates if the object was found and 
     * the pose of the object in the tcp frame.
     */
    std::tuple<bool, geometry_msgs::msg::Pose> XarmClient::get_object_pose(std::string object_name)
    {
        auto request = std::make_shared<xarm_interfaces::srv::GetObjectPose::Request>();
        request->object_name = object_name;
        auto result_future = get_object_pose_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();

            if (!result->is_object_found)
            {
                return {false, geometry_msgs::msg::Pose()};
            }

            geometry_msgs::msg::TransformStamped object_transform = result->object_transform;
            geometry_msgs::msg::Pose pose = geometry_msgs::msg::Pose();

            // if object is not a container and is already in the container zone, ignore it.
            if (object_name != "box" && object_name != "plate")
            {   
                // transform the object pose from camera frame into arm_base frame
                try
                {
                    auto t = tf_buffer_->transform(object_transform, "arm_base", tf2::durationFromSec(0.5));
                    // check if the object is in the container zone. This check is only valid for the current setup.
                    if (t.transform.translation.x >= 0.15)
                    {   
                        return {false, geometry_msgs::msg::Pose()};
                    }
                }
                catch (const tf2::TransformException &ex)
                {
                    RCLCPP_INFO(node_->get_logger(), "Could not transform object from camera into arm_base frame: %s", ex.what());
                    return {false, geometry_msgs::msg::Pose()};
                }
            }

            // transform the object pose from camera frame to tcp frame
            try
            {
                auto t = tf_buffer_->transform(object_transform, "tcp", tf2::durationFromSec(0.5));

                pose.position.x = t.transform.translation.x;
                pose.position.y = t.transform.translation.y;
                pose.position.z = t.transform.translation.z;
                pose.orientation.x = t.transform.rotation.x;
                pose.orientation.y = t.transform.rotation.y;
                pose.orientation.z = t.transform.rotation.z;
                pose.orientation.w = t.transform.rotation.w;
            }
            catch (const tf2::TransformException &ex)
            {
                RCLCPP_INFO(node_->get_logger(), "Could not transform object from camera into tcp frame: %s", ex.what());
                return {false, geometry_msgs::msg::Pose()};
            }

            return {true, pose};
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_object_pose");
            return {false, geometry_msgs::msg::Pose()};
        }
    }

    /**
     * @brief Send a command to the gripper.
     * 
     * @param order "open" or "close".
     * @param position The position to which the gripper should move. This parameter is only used when the order is "open".
     * @return True if the command was sent successfully, false otherwise.
     */
    bool XarmClient::set_gripper_order(std::string order, int position)
    {
        auto request = std::make_shared<xarm_interfaces::srv::SetGripperOrder::Request>();
        request->order = order;
        request->position = position;
        auto result_future = set_gripper_order_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            return result->success;
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service set_gripper_order");
            return false;
        }
    }

    /**
     * @brief Get the angle in degrees by how much the gripper should rotate to pick the object.
     * 
     * @param object_name The name of the object to find.
     * @return std::tuple<bool, float> A tuple containing a boolean that indicates if the object was found and 
     * the angle in degrees by how much the gripper should rotate to pick the object.
     */
    std::tuple<bool, float> XarmClient::get_gripper_angle(std::string object_name)
    {
        auto request = std::make_shared<xarm_interfaces::srv::GetGripperAngle::Request>();
        request->object_name = object_name;
        auto result_future = get_gripper_angle_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            return {result->is_object_found, result->gripper_angle};
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_gripper_angle");
            return {false, 0.0};
        }
    }

    /**
     * @brief Update the transform from tcp to tcp_target frame in order to let tf2 compute the transform from
     * arm_base to end_effector_target frame.
     * 
     * @param target_tf The transform from tcp to tcp_target frame.
     * @return True if the transform was updated successfully, false otherwise.
     */
    bool XarmClient::set_target_tf(geometry_msgs::msg::TransformStamped target_tf)
    {
        auto request = std::make_shared<xarm_interfaces::srv::SetTargetTf::Request>();
        request->target_transform = target_tf;
        auto result_future = set_target_tf_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            return result->success;
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service set_target_tf");
            return false;
        }
    }

    /**
     * @brief Check if the gripper is empty.
     * 
     * @note The gripper is considered empty if the load is too small or the position is too much closed.
     * However the values used are not always valid depending on the default close position and the accuracy of the load sensor
     * in the Dynamixel. Therefore it should be used with caution and updated with each new gripper.
     * 
     * @return True if the gripper is empty, false otherwise.
     */
    bool XarmClient::is_gripper_empty(){
        auto load = gripper_client_.get_load_loop();
        auto position = gripper_client_.get_position();
        // return load <= 50 || position <= -5000;
        return false;
    }
}