#include "xarm_main_control/xarm_main_control.hpp"
#include "xarm_main_control/xarm_main_planner.hpp"
#include "std_msgs/msg/bool.hpp"
#include "std_msgs/msg/string.hpp"

using std::placeholders::_1;
/**
 * @brief A class that instantiates a ROS 2 node to control the xArm robot.
 * 
 */
class XarmMainControl : public rclcpp::Node
{
public:
    xarm_main_control::XarmMainPlanner * xarm_main_planner_;
private:
    xarm_main_control::XarmClient xarm_client_;
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr object_name_publisher_;

public:
    /**
     * @brief Construct a new Xarm Main Control object.
     * 
     * @param node_options The options for the node.
     */
    XarmMainControl(const rclcpp::NodeOptions &node_options)
        : Node("xarm_main_control", node_options), xarm_main_planner_(), xarm_client_("xarm_main_control_client")
    {
        int dof;
        this->get_parameter_or("dof", dof, 7);
        std::string robot_type;
        this->get_parameter_or("robot_type", robot_type, std::string("xarm"));
        auto group_name = robot_type + std::to_string(dof);

        RCLCPP_INFO(this->get_logger(), "namespace: %s, group_name: %s", this->get_namespace(), group_name.c_str());

        object_name_publisher_ = this->create_publisher<std_msgs::msg::String>("object_name", 10);

        // while (object_name_publisher_->get_subscription_count() == 0)
        // {
        //     RCLCPP_INFO(this->get_logger(), "waiting for subscriber...");
        //     sleep(1);
        // }
        // sleep for 2 seconds to wait for all the tf to be broadcasted
        sleep(2);
    }

public:
    /**
     * @brief The main control function that executes the mission.
     * 
     * @return True if the mission was successful, false otherwise.
     */
    bool main_control()
    {
        ///////////////////////////////////// Calibration /////////////////////////////////////

        // std::vector<float> joint = xarm_main_planner_->get_robot_joints();
        // RCLCPP_INFO(this->get_logger(), "joints: %f, %f, %f, %f, %f, %f", joint[0], joint[1], joint[2], joint[3], joint[4], joint[5]);

        // xarm_main_planner_->execute_joint_target(search_object_joint_target_.at("horizontal"));
        // xarm_main_planner_->execute_joint_target(search_object_joint_target_.at("vertical"));

        // xarm_main_planner_->execute_delta_target(0.08, 0, 0);
        // xarm_main_planner_->execute_delta_target(0, 0.08, 0);

        // xarm_main_planner_->execute_joint_target(rest_joint_target_);

        // auto object_name = "bottle";
        // publish_object_name(object_name);

        // auto [is_object_found, pose] = get_object_pose_loop(object_name, 1, false);
        // while(true){
        //     std::tie(is_object_found, pose) = get_object_pose_loop(object_name, 1, false);
        //     if(is_object_found){
        //         RCLCPP_INFO(this->get_logger(), "x: %f, y: %f, z: %f, qx: %f, qy: %f, qz: %f, qw: %f", pose.position.x, pose.position.y, pose.position.z, pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w);
        //         target_object(object_name, object_orientation_.at(object_name));
        //         std::tie(is_object_found, pose) = get_object_pose_loop(object_name, 5, false);
        //         RCLCPP_INFO(this->get_logger(), "x: %f, y: %f, z: %f, qx: %f, qy: %f, qz: %f, qw: %f", pose.position.x, pose.position.y, pose.position.z, pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w);
        //         pick_object(object_name, object_orientation_.at(object_name));
        //         break;
        //     }
        //     sleep(1);
        // }

        // search_target_and_pick_object(object_name, 2);

        ///////////////////////////////////// Real Code ///////////////////////////////////////////

        xarm_client_.set_gripper_order("open", search_opening_);
        xarm_main_planner_->execute_joint_target(rest_joint_target_);

        RCLCPP_INFO(this->get_logger(), "Starting mission to save containers positions ...");
        for (std::string container_name : container_order_)
        {
            if (!search_target_and_save_container(container_name, 2))
            {
                RCLCPP_INFO(this->get_logger(), "Failed to save containers positions, ending mission ...");
                xarm_main_planner_->execute_joint_target(rest_joint_target_);
                return false;
            }
        }

        RCLCPP_INFO(this->get_logger(), "Starting mission to fill containers ...");
        auto default_search_object_joint_target = search_object_joint_target_.at("horizontal");
        xarm_main_planner_->execute_joint_target(default_search_object_joint_target);

        for (std::string object_name : object_order_)
        {
            int nb_times_to_pick_object = object_quantity_.at(object_name);

            for (int i = 0; i < nb_times_to_pick_object; i++)
            {
                bool is_object_picked = search_target_and_pick_object(object_name, 2);
                xarm_main_planner_->execute_joint_target(default_search_object_joint_target);
                if (!is_object_picked)
                {
                    break;
                }
                if (!fill_container(object_name))
                {
                    RCLCPP_INFO(this->get_logger(), "Failed to fill container with %s, trying again ...", object_name.c_str());
                    i --;
                }
                xarm_main_planner_->execute_joint_target(default_search_object_joint_target);
            }

            RCLCPP_INFO(this->get_logger(), "All the %s were picked and placed", object_name.c_str());
        }

        RCLCPP_INFO(this->get_logger(), "End of mission to fill containers!");
        xarm_main_planner_->execute_joint_target(rest_joint_target_);
        return true;
    }

private:
    /**
     * @brief Searches for the container, targets it and saves its position.
     * 
     * @param container_name The name of the container to search.
     * @param nb_trials The number of times to try to find the container.
     * @return True if the container was found and saved, false otherwise.
     */
    bool search_target_and_save_container(std::string container_name, int nb_trials)
    {
        bool is_container_saved = false;
        int search_container_step = 0;
        auto search_container_sequence = search_object_sequence_.at("container");
        auto search_container_joint_target = search_object_joint_target_.at("container");

        publish_object_name(container_name);
        xarm_main_planner_->execute_joint_target(search_container_joint_target);
        sleep(2);

        int counter = 0;
        RCLCPP_INFO(this->get_logger(), "Starting search for %s", container_name.c_str());
        while (!is_container_saved)
        {
            if (search_container_step == search_container_sequence.size())
            {
                search_container_step = 0;
                xarm_main_planner_->execute_joint_target(search_container_joint_target);
                counter++;
                if (counter >= nb_trials)
                {
                    RCLCPP_INFO(this->get_logger(), "Failed to find %s, aborting mission ...", container_name.c_str());
                    return false;
                }

                RCLCPP_INFO(this->get_logger(), "Failed to find %s, starting search again ...", container_name.c_str());
                // move the robot up to try to find the container again
                xarm_main_planner_->execute_delta_target(0, 0, -0.02 * counter);
            }

            auto [is_container_found, pose] = get_object_pose_loop(container_name, 1, false);
            if (!is_container_found)
            {
                // go to next container search sequence step
                xarm_main_planner_->execute_sequence_step(search_container_sequence[search_container_step]);
                search_container_step++;
                continue;
            }

            // Saves the current pose of the robot in case targeting the container fails
            std::vector<float> container_found_xarm_pose = xarm_main_planner_->get_robot_joints();

            RCLCPP_INFO(this->get_logger(), "%s found, start targeting ...", container_name.c_str());

            if (!target_object(container_name, "horizontal"))
            {
                xarm_main_planner_->execute_joint_target(container_found_xarm_pose);
                // go to next container search sequence step
                xarm_main_planner_->execute_sequence_step(search_container_sequence[search_container_step]);
                search_container_step++;
                continue;
            }

            // save the position of the container
            auto container_pose = xarm_main_planner_->get_robot_joints();
            container_pose_[container_name] = container_pose;
            is_container_saved = true;
        }

        RCLCPP_INFO(this->get_logger(), "%s saved!", container_name.c_str());
        xarm_main_planner_->execute_joint_target(search_container_joint_target);
        return true;
    }

private:
    /**
     * @brief Targets the object by ensuring that the TCP of the arm is aligned with the center of the object at
     * a specific distance from it.
     * 
     * @param object_name The name of the object to target.
     * @param object_orientation The orientation of the object to target.
     * @return True if the object was targeted, false otherwise.
     */
    bool target_object(std::string object_name, std::string object_orientation)
    {
        auto [is_object_found, object_pose] = get_object_pose_loop(object_name, 5);
        if (!is_object_found)
        {
            return false;
        }

        float x = object_pose.position.x;
        float y = object_pose.position.y;
        float z = object_pose.position.z;
        // if object is vertical we stay at the same height to avoid hitting the table
        if (object_orientation == "vertical")
        {
            x = 0;
        }

        float xc = 0;
        float yc = 0;
        // we want to target the object at 20 cm from its center
        float zc = 0.20;
        if (object_orientation == "vertical")
        {
            zc = 0.10;
        }

        while (!all_close(x, y, z, xc, yc, zc))
        {
            xarm_main_planner_->execute_delta_target(x, y, z - zc);

            std::tie(is_object_found, object_pose) = get_object_pose_loop(object_name, 5);
            if (!is_object_found)
            {
                return false;
            }

            x = object_pose.position.x;
            y = object_pose.position.y;
            z = object_pose.position.z;

            if (object_orientation == "vertical")
            {
                x = 0;
            }
        }
        return true;
    }

private:
    /**
     * @brief Fills the container with the object.
     * 
     * @param object_name The name of the object to fill the container with.
     * @return True if the container was filled, false otherwise.
     */
    bool fill_container(std::string object_name)
    {   
        auto container_name = object_container_.at(object_name);
        auto success = xarm_main_planner_->execute_joint_target(container_pose_[container_name]);
        RCLCPP_INFO(this->get_logger(), "Filling %s with %s", container_name.c_str(), object_name.c_str());
        
        if(xarm_client_.is_gripper_empty()){
            RCLCPP_INFO(this->get_logger(), "Gripper is empty, %s must have fallen", object_name.c_str());
            return false;
        }

        float z = 0.10;

        success &= xarm_main_planner_->execute_delta_target(0, 0, z);
        success &= xarm_client_.set_gripper_order("open", release_opening_);
        // sleeps to let the object fall into the container
        sleep(1);
        success &= xarm_main_planner_->execute_delta_target(0, 0, -z);
        success &= xarm_client_.set_gripper_order("open", search_opening_);
        success &= xarm_main_planner_->execute_joint_target(container_pose_[container_name]);
        return success;
    }

private:
    /**
     * @brief Picks the object.
     * 
     * @param object_name The name of the object to pick.
     * @param object_orientation The orientation of the object to pick.
     * @return True if the object was picked, false otherwise.
     */
    bool pick_object(std::string object_name, std::string object_orientation)
    {
        bool success = true;

        auto [is_object_found, object_pose] = get_object_pose_loop(object_name, 5);
        if (!is_object_found)
        {
            return false;
        }

        if (object_orientation == "horizontal")
        {
            auto [is_object_found, angle] = get_gripper_angle_loop(object_name, 5);
            if (!is_object_found)
            {
                return false;
            }
            // rotate the gripper along the z axis of the end effector, to be aligned with the object
            success &= xarm_main_planner_->execute_rotation_target(0, 0, 1, angle);
        }

        float d;
        float z = object_pose.position.z;
        if (object_orientation == "horizontal")
        {
            z += 0.02;
            d = 0.1;
        }
        // vertical case we go farther to grab entirely the object
        else
        {
            z += 0.08;
            d = 0.18;
        }
        success &= xarm_main_planner_->execute_delta_target(0.0, 0.0, z - d);
        success &= xarm_client_.set_gripper_order("open", pick_opening_.at(object_name));
        success &= xarm_main_planner_->execute_delta_target(0.0, 0.0, d);
        success &= xarm_client_.set_gripper_order("close");
        success &= xarm_main_planner_->execute_delta_target(0.0, 0.0, -z);
        if(xarm_client_.is_gripper_empty()){
            RCLCPP_INFO(this->get_logger(), "Gripper is empty, %s must have fallen", object_name.c_str());
            return false;
        }
        return success;
    }

private:
    /**
     * @brief Searches for the object, targets it and picks it.
     * 
     * @param object_name The name of the object to search and pick.
     * @param nb_trials The number of times to try to find the object.
     * @return True if the object was picked, false otherwise.
     */
    bool search_target_and_pick_object(std::string object_name, int nb_trials)
    {
        bool is_object_picked = false;
        int search_object_step = 0;
        std::string object_orientation = object_orientation_.at(object_name);
        auto search_object_sequence = search_object_sequence_.at(object_orientation);
        auto search_object_joint_target = search_object_joint_target_.at(object_orientation);

        publish_object_name(object_name);
        xarm_main_planner_->execute_joint_target(search_object_joint_target);
        sleep(2);

        RCLCPP_INFO(this->get_logger(), "Starting search for %s", object_name.c_str());
        while (!is_object_picked)
        {
            if (search_object_step == search_object_sequence.size())
            {
                search_object_step = 0;
                xarm_main_planner_->execute_joint_target(search_object_joint_target);
                nb_trials--;
                if (nb_trials <= 0)
                {
                    RCLCPP_INFO(this->get_logger(), "Failed to pick and place all the %s, going to the next object", object_name.c_str());
                    return false;
                }
                RCLCPP_INFO(this->get_logger(), "Failed to pick %s, starting search again ...", object_name.c_str());
            }

            auto [is_object_found, pose] = get_object_pose_loop(object_name, 1, false);
            if (!is_object_found)
            {
                // go to the next object search sequence step
                xarm_main_planner_->execute_sequence_step(search_object_sequence[search_object_step]);
                search_object_step++;
                continue;
            }

            // Saves the current pose of the robot in case targeting the object fails
            std::vector<float> object_found_xarm_pose = xarm_main_planner_->get_robot_joints();

            RCLCPP_INFO(this->get_logger(), "%s found, start targeting ...", object_name.c_str());

            if (!target_object(object_name, object_orientation))
            {
                xarm_main_planner_->execute_joint_target(object_found_xarm_pose);
                // go to the next object search sequence step
                xarm_main_planner_->execute_sequence_step(search_object_sequence[search_object_step]);
                search_object_step++;
                continue;
            }

            RCLCPP_INFO(this->get_logger(), "%s successfully targeted, start picking ...", object_name.c_str());

            is_object_picked = pick_object(object_name, object_orientation);
            if (!is_object_picked)
            {
                xarm_main_planner_->execute_joint_target(object_found_xarm_pose);
                // go to the next object search sequence step
                xarm_main_planner_->execute_sequence_step(search_object_sequence[search_object_step]);
                search_object_step++;
                continue;
            }

            xarm_main_planner_->execute_joint_target(object_found_xarm_pose);
        }

        RCLCPP_INFO(this->get_logger(), "%s successfully picked", object_name.c_str());
        xarm_main_planner_->execute_joint_target(search_object_joint_target);
        return true;
    }

private:
    /**
     * @brief Checks if the current position of the TCP is close to the target position.
     * 
     * @param x The current x position of the TCP in meters.
     * @param y The current y position of the TCP in meters.
     * @param z The current z position of the TCP in meters.
     * @param xc The target x position of the TCP in meters.
     * @param yc The target y position of the TCP in meters.
     * @param zc The target z position of the TCP in meters.
     * @param tolerance How close the current position of the TCP should be to the target position at minimum in meters.
     * @return True if the current position of the TCP is close enough to the target position, false otherwise.
     */
    bool all_close(float x, float y, float z, float xc, float yc, float zc, float tolerance = 0.01)
    {
        return (abs(x - xc) <= tolerance) && (abs(y - yc) <= tolerance) && (abs(z - zc) <= tolerance);
    }

private:
    /**
     * @brief Publishes the name of the object to detect to the object_name topic.
     * 
     * @param object_name The name of the object to detect.
     */
    void publish_object_name(std::string object_name)
    {
        auto msg = std_msgs::msg::String();
        msg.data = object_name;
        object_name_publisher_->publish(msg);
        RCLCPP_INFO(this->get_logger(), "published object name: %s", object_name.c_str());
    }

private:
    /**
     * @brief Get the object pose in the TCP frame by looping at max nb_trials times to try to find the object.
     * 
     * @param object_name The name of the object to get the pose of.
     * @param nb_trials The number of times to try to find the object.
     * @param message_on Whether to print messages or not.
     * @return std::tuple<bool, geometry_msgs::msg::Pose> The pose of the object in the TCP frame.
     */
    std::tuple<bool, geometry_msgs::msg::Pose> get_object_pose_loop(std::string object_name, int nb_trials = 1, bool message_on = true)
    {
        auto [is_object_found, pose] = xarm_client_.get_object_pose(object_name);
        int i = 1;
        while (!is_object_found && i < nb_trials)
        {
            if (message_on)
            {
                RCLCPP_INFO(this->get_logger(), "%s lost, trying to find it again ...", object_name.c_str());
            }
            // move the robot a little bit to try to find the object again
            xarm_main_planner_->execute_delta_target(0, 0.005, -0.005);
            std::tie(is_object_found, pose) = xarm_client_.get_object_pose(object_name);
            i++;
            sleep(1);
        }
        return std::make_tuple(is_object_found, pose);
    }

private:
    /**
     * @brief Get the angle in degrees by how much the gripper should rotate to pick the object
     *  by looping at max nb_trials times to try to find the object.
     * 
     * @param object_name The name of the object to pick.
     * @param nb_trials The number of times to try to find the object.
     * @return std::tuple<bool, float> The angle in degrees by how much the gripper should rotate to pick the object.
     */
    std::tuple<bool, float> get_gripper_angle_loop(std::string object_name, int nb_trials = 1)
    {
        auto [is_object_found, angle] = xarm_client_.get_gripper_angle(object_name);
        int i = 1;
        while (!is_object_found && i < nb_trials)
        {
            RCLCPP_INFO(this->get_logger(), "%s lost, trying to find it again ...", object_name.c_str());
            // move the robot a little bit to try to find the object again
            xarm_main_planner_->execute_delta_target(0, 0.005, -0.005);
            std::tie(is_object_found, angle) = xarm_client_.get_gripper_angle(object_name);
            i++;
            sleep(1);
        }
        return std::make_tuple(is_object_found, angle);
    }
};

/**
 * @brief Main function of the xarm_main_control file that initializes and starts spinning the ROS 2 node.
 *
 * @param argc The number of arguments.
 * @param argv The arguments.
 * @return int The exit code.
 */
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    rclcpp::NodeOptions node_options;
    node_options.automatically_declare_parameters_from_overrides(true);

    std::shared_ptr<XarmMainControl> xarm_node = std::make_shared<XarmMainControl>(node_options);

    rclcpp::executors::SingleThreadedExecutor executor;
    executor.add_node(xarm_node);
    std::thread([&executor]() { executor.spin(); }).detach();

    RCLCPP_INFO(xarm_node->get_logger(), "xarm_main_control start");

    xarm_main_control::XarmMainPlanner planner(xarm_node);
    xarm_node->xarm_main_planner_ = &planner;

    xarm_node->main_control();

    RCLCPP_INFO(xarm_node->get_logger(), "xarm_main_control over");
    rclcpp::shutdown();
    return 0;
}