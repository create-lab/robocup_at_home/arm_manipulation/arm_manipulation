#include "xarm_main_control/xarm_main_planner.hpp"

namespace xarm_main_control
{
    /**
     * @brief Construct a dummy new Xarm Main Planner:: Xarm Main Planner object to
     * initialize an object of this class.
     * You should always use the other constructor with the node parameter, after having initialized
     * the ROS 2 node in the main function that will run in a separate thread. Otherwise, moveit2
     * will not be able to retrieve the robot pose and joint angles.
     *
     */
    XarmMainPlanner::XarmMainPlanner()
        : xarm_client_("xarm_main_planner_client"), planner_("xarm6")
    {
        node_ = rclcpp::Node::make_shared("xarm_main_planner");
        tf_buffer_ = std::make_unique<tf2_ros::Buffer>(node_->get_clock());
        tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

        RCLCPP_INFO(rclcpp::get_logger("xarm_main_planner"), "xarm_main_planner is ready");
    }

    /**
     * @brief Construct a new Xarm Main Planner:: Xarm Main Planner object
     *
     * @param node The ROS 2 node that will be used to instantiate the planner.
     */

    XarmMainPlanner::XarmMainPlanner(const rclcpp::Node::SharedPtr& node)
        : xarm_client_("xarm_main_planner_client"), planner_(node, "xarm6")

    {
        node_ = rclcpp::Node::make_shared("xarm_main_planner");
        tf_buffer_ = std::make_unique<tf2_ros::Buffer>(node_->get_clock());
        tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

        RCLCPP_INFO(rclcpp::get_logger("xarm_main_planner"), "xarm_main_planner is ready");
    }

    /**
     * @brief Returns the pose of the end_effector in the xarm_base frame with the 3D coordinates
     * in meters and a quaternion.
     *
     * @return geometry_msgs::msg::Pose The pose of the end_effector in the xarm_base frame.
     *
     */
    geometry_msgs::msg::Pose XarmMainPlanner::get_robot_pose()
    {
        return planner_.getCurrentPose().pose;
    }

    /**
     * @brief Returns the joint angles of the arm in radians.
     *
     * @return std::vector<float> The joint angles of the arm in radians.
     */
    std::vector<float> XarmMainPlanner::get_robot_joints()
    {
        auto joints = planner_.getCurrentJointValues();
        std::vector<float> float_joints(joints.begin(), joints.end());
        return float_joints;
    }

    /**
     * @brief Execute a joint target goal given as a vector of 6 joint angles in radians.
     *
     * @param joint_target The joint target goal given as a vector of 6 joint angles in radians.
     * @return True if the goal is reached, false otherwise.
     */
    bool XarmMainPlanner::execute_joint_target(std::vector<float> joint_target)
    {
        std::vector<double> double_joint_target(joint_target.begin(), joint_target.end());
        planner_.planJointTarget(double_joint_target);
        return planner_.executePath();
    }

    /**
     * @brief Execute a delta target movement from the current TCP position, given as a vector of 3 coordinates in meters.
     *
     * @note The default plan is a Cartesian path, if it fails, a joint target path is planned.
     *
     * @param dx Delta x displacement in meters.
     * @param dy Delta y displacement in meters.
     * @param dz Delta z displacement in meters.
     * @return True if the goal is reached, false otherwise.
     */
    bool XarmMainPlanner::execute_delta_target(float dx, float dy, float dz)
    {
        auto target_pose = get_robot_pose();

        // create the transform from tcp frame to tcp_target frame
        geometry_msgs::msg::TransformStamped tcp_transform = geometry_msgs::msg::TransformStamped();
        tcp_transform.header.stamp = node_->get_clock()->now();
        tcp_transform.header.frame_id = "tcp";
        tcp_transform.child_frame_id = "tcp_target";
        tcp_transform.transform.translation.x = dx;
        tcp_transform.transform.translation.y = dy;
        tcp_transform.transform.translation.z = dz;
        // the target here is a 3D point, so the orientation is not important
        tcp_transform.transform.rotation.x = 0;
        tcp_transform.transform.rotation.y = 0;
        tcp_transform.transform.rotation.z = 0;
        tcp_transform.transform.rotation.w = 1;

        // check if the tcp target will not hit the table, otherwise set the z to the table height
        try
        {
            auto t1 = tf_buffer_->transform(tcp_transform, "arm_base", tf2::durationFromSec(0.5));
            if (t1.transform.translation.z < z_table_)
            {
                t1.transform.translation.z = z_table_;
                try
                {
                    auto t2 = tf_buffer_->transform(t1, "tcp", tf2::durationFromSec(0.5));
                    tcp_transform.transform.translation = t2.transform.translation;
                }
                catch (const tf2::TransformException &ex)
                {
                    RCLCPP_INFO(node_->get_logger(), "Could not transform  t1 into tcp frame : %s", ex.what());
                    return false;
                }
            }
        }
        catch (const tf2::TransformException &ex)
        {
            RCLCPP_INFO(node_->get_logger(), "Could not transform tcp_transform into arm_base frame : %s", ex.what());
            return false;
        }

        // update the transform from tcp frame to tcp_target frame in order to let tf2 compute the
        // transform from arm_base frame to end_effector_target frame
        if (!xarm_client_.set_target_tf(tcp_transform))
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to set tcp target transform");
            return false;
        }
        // sleep for 0.1s to wait for the transform to be published
        sleep(0.1);

        // compute the transform from arm_base frame to end_effector_target frame, which is equivalent to
        // transform the end_effector_tartget frame into the arm_base frame.
        try
        {
            geometry_msgs::msg::TransformStamped t;
            t = tf_buffer_->lookupTransform("arm_base", "end_effector_target", node_->get_clock()->now(), 500ms);

            target_pose.position.x = t.transform.translation.x;
            target_pose.position.y = t.transform.translation.y;
            target_pose.position.z = t.transform.translation.z;
        }
        catch (const tf2::TransformException &ex)
        {
            RCLCPP_INFO(node_->get_logger(), "Could not transform end_effector_target frame into arm_base frame: %s", ex.what());
            return false;
        }

        // if Cartesian path planning fails, plan a joint target path because it is less restrictive
        planner_.planCartesianPath(std::vector<geometry_msgs::msg::Pose_<std::allocator<void>>>{target_pose});
        auto success = planner_.executePath();
        if (!success)
        {
            planner_.planPoseTarget(target_pose);
            success = planner_.executePath();
        }
        return success;
    }

    /**
     * @brief Execute a rotation target movement around the axis given as a vector of 3 coordinates in meters and an angle in degrees.
     * Be careful that the rotation is not around the TCP, but around the end effector.
     *
     * @note The rotation is not around the TCP, but around the end effector.
     * @note The default plan is a Cartesian path, if it fails, a joint target path is planned.
     *
     * @param xx The x coordinate of the axis of rotation in the end effector frame.
     * @param yy The y coordinate of the axis of rotation in the end effector frame.
     * @param zz The z coordinate of the axis of rotation in the end effector frame.
     * @param a The angle of rotation in degrees by how much the end effector should rotate.
     * @return True if the goal is reached, false otherwise.
     */
    bool XarmMainPlanner::execute_rotation_target(float xx, float yy, float zz, float a)
    {
        auto target_pose = get_robot_pose();

        // convert the axis of rotation coordinates from the end effector frame to the arm_base frame
        auto base_axis_coord = convert_delta_local_to_base_frame(xx, yy, zz);

        auto q_rot = create_quaternion_from_axis_angle(base_axis_coord.x(), base_axis_coord.y(), base_axis_coord.z(), a);

        tf2::Quaternion q_end_effector = tf2::Quaternion(target_pose.orientation.x, target_pose.orientation.y,
                                                         target_pose.orientation.z, target_pose.orientation.w);

        auto q_final = q_rot * q_end_effector;
        q_final.normalize();

        target_pose.orientation.x = q_final.x();
        target_pose.orientation.y = q_final.y();
        target_pose.orientation.z = q_final.z();
        target_pose.orientation.w = q_final.w();

        // if Cartesian path planning fails, plan a joint target path because it is less restrictive.
        planner_.planCartesianPath(std::vector<geometry_msgs::msg::Pose_<std::allocator<void>>>{target_pose});
        auto success = planner_.executePath();
        if (!success)
        {
            planner_.planPoseTarget(target_pose);
            success = planner_.executePath();
        }
        return success;
    }

    /**
     * @brief Rotate the end effector around the z axis of the end effector frame.
     *
     * @note The direction of rotation is dependent on the orientation of the end effector, so if
     * you want something that is not dependent on the orientation, use "execute_rotation_target(0, 0, 1, angle)" instead.
     *
     * @param angle The angle of rotation in degrees by how much the end effector should rotate.
     * @return True if the goal is reached, false otherwise.
     */
    bool XarmMainPlanner::rotate_end_effector(float angle)
    {
        auto joint = get_robot_joints();
        std::vector<float> joint_target = {joint[0], joint[1], joint[2], joint[3],
                                           joint[4], joint[5] + angle * static_cast<float>(DEGREE_TO_RADIAN)};
        return execute_joint_target(joint_target);
    }

    /**
     * @brief Rotate the base of the robot around the z axis of the arm_base frame.
     *
     * @param angle The angle of rotation in degrees by how much the base should rotate.
     * @return True if the goal is reached, false otherwise.
     */
    bool XarmMainPlanner::rotate_base(float angle)
    {
        auto joint = get_robot_joints();
        std::vector<float> joint_target = {joint[0] + angle * static_cast<float>(DEGREE_TO_RADIAN), joint[1], joint[2], joint[3],
                                           joint[4], joint[5]};
        return execute_joint_target(joint_target);
    }

    /**
     * @brief Execute the right movement given the size of the sequence_step vector. This method is used
     * to loop on a sequence of different movements, like a search sequence for example.
     *
     * @param sequence_step The vector representing the next sequence_step movement to execute.
     * @return True if the sequence_step was executed successfully, false otherwise.
     */
    bool XarmMainPlanner::execute_sequence_step(std::vector<float> sequence_step)
    {
        if (sequence_step.size() == 3)
        {
            return execute_delta_target(sequence_step[0], sequence_step[1], sequence_step[2]);
        }
        else if (sequence_step.size() == 4)
        {
            return execute_rotation_target(sequence_step[0], sequence_step[1], sequence_step[2], sequence_step[3]);
        }
        else if (sequence_step.size() == 1)
        {
            return rotate_end_effector(sequence_step[0]);
        }
        else
        {
            RCLCPP_ERROR(rclcpp::get_logger("xarm_main_planner"), "sequence size error");
            return false;
        }
    }

    /**
     * @brief Convert a delta vector from the end effector frame to the arm_base frame.
     *
     * @param dx The delta x coordinate in the end effector frame in meters.
     * @param dy The delta y coordinate in the end effector frame in meters.
     * @param dz The delta z coordinate in the end effector frame in meters.
     * @return tf2::Vector3 The delta vector in the arm_base frame in meters.
     */
    tf2::Vector3 XarmMainPlanner::convert_delta_local_to_base_frame(float dx, float dy, float dz)
    {
        auto pose_msg = get_robot_pose();
        tf2::Quaternion q_end_effector = tf2::Quaternion(pose_msg.orientation.x, pose_msg.orientation.y, pose_msg.orientation.z, pose_msg.orientation.w);
        q_end_effector.normalize();
        auto rot_mat = tf2::Matrix3x3(q_end_effector);
        auto local_coord_dm = tf2::Vector3(dx, dy, dz);
        auto base_coord_dm = rot_mat * local_coord_dm;
        return base_coord_dm;
    }

    /**
     * @brief Creates a quaternion from an axis of rotation and an angle of rotation.
     *
     * @param xx The x coordinate of the axis of rotation.
     * @param yy The y coordinate of the axis of rotation.
     * @param zz The z coordinate of the axis of rotation.
     * @param a The angle of rotation in degrees.
     * @return tf2::Quaternion
     */
    tf2::Quaternion XarmMainPlanner::create_quaternion_from_axis_angle(float xx, float yy, float zz, float a)
    {
        // Here we calculate the sin( theta / 2) once for optimization
        auto factor = sin(a * DEGREE_TO_RADIAN / 2.0);

        // Calculate the x, y and z of the quaternion
        auto x = xx * factor;
        auto y = yy * factor;
        auto z = zz * factor;

        // Calcualte the w value by cos( theta / 2 )
        auto w = cos(a * DEGREE_TO_RADIAN / 2.0);

        return tf2::Quaternion(x, y, z, w).normalize();
    }
}
