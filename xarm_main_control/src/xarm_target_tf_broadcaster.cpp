#include "xarm_main_control/xarm_client.hpp"
#include "tf2_ros/transform_broadcaster.h"
#include "xarm_interfaces/srv/set_target_tf.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
using std::placeholders::_2;

/**
 * @brief A class that instantiates a ROS 2 node to compute and broadcast the transform
 * from the tcp_target to the end_effector_target frame.
 * 
 */
class XarmTargetTfBroadcaster : public rclcpp::Node
{
private:
    rclcpp::TimerBase::SharedPtr timer_{nullptr};
    std::unique_ptr<tf2_ros::TransformBroadcaster> tf_broadcaster_{nullptr};
    std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
    std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
    geometry_msgs::msg::TransformStamped tcp_transform_;
    geometry_msgs::msg::TransformStamped end_effector_transform_;
    rclcpp::Service<xarm_interfaces::srv::SetTargetTf>::SharedPtr set_target_tf_service_;

public:
    /**
     * @brief Construct a new Xarm Target Tf Broadcaster object
     * 
     */
    XarmTargetTfBroadcaster()
        : Node("xarm_target_tf_broadcaster")
    {
        tf_broadcaster_ = std::make_unique<tf2_ros::TransformBroadcaster>(*this);

        tf_buffer_ = std::make_unique<tf2_ros::Buffer>(this->get_clock());

        tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

        set_target_tf_service_ = this->create_service<xarm_interfaces::srv::SetTargetTf>(
            "set_target_tf", std::bind(&XarmTargetTfBroadcaster::set_target_tf_callback, this, _1, _2));

        // create a timer that calls the callback every 50ms
        timer_ = this->create_wall_timer(
            50ms, std::bind(&XarmTargetTfBroadcaster::timer_callback, this));

        // initialize the transforms
        tcp_transform_.header.stamp = this->get_clock()->now();
        tcp_transform_.header.frame_id = "tcp";
        tcp_transform_.child_frame_id = "tcp_target";
        tcp_transform_.transform.translation.x = 0;
        tcp_transform_.transform.translation.y = 0;
        tcp_transform_.transform.translation.z = 0;
        tcp_transform_.transform.rotation.x = 0;
        tcp_transform_.transform.rotation.y = 0;
        tcp_transform_.transform.rotation.z = 0;
        tcp_transform_.transform.rotation.w = 1;

        end_effector_transform_.header.stamp = this->get_clock()->now();
        end_effector_transform_.header.frame_id = "tcp_target";
        end_effector_transform_.child_frame_id = "end_effector_target";
        end_effector_transform_.transform.translation.x = 0;
        end_effector_transform_.transform.translation.y = 0;
        end_effector_transform_.transform.translation.z = 0;
        end_effector_transform_.transform.rotation.x = 0;
        end_effector_transform_.transform.rotation.y = 0;
        end_effector_transform_.transform.rotation.z = 0;
        end_effector_transform_.transform.rotation.w = 1;

        RCLCPP_INFO(this->get_logger(), "xarm_target_tf_broadcaster is ready.");
    }

private:
    /**
     * @brief Callback function for the timer, that computes and broadcasts the transform from the tcp_target 
     * to the end_effector_target frame.
     * 
     */
    void timer_callback()
    {
        tcp_transform_.header.stamp = this->get_clock()->now();
        tf_broadcaster_->sendTransform(tcp_transform_);

        geometry_msgs::msg::TransformStamped transform_from_end_effector_to_tcp;
        try
        {
            transform_from_end_effector_to_tcp = tf_buffer_->lookupTransform("tcp", "link_eef", this->get_clock()->now(), 500ms);
        }
        catch (const tf2::TransformException &ex)
        {
            RCLCPP_INFO(this->get_logger(), "Could not transform end_effector into tcp frame: %s", ex.what());
            return;
        }

        end_effector_transform_.header.stamp = this->get_clock()->now();
        end_effector_transform_.transform.translation = transform_from_end_effector_to_tcp.transform.translation;
        end_effector_transform_.transform.rotation = transform_from_end_effector_to_tcp.transform.rotation;
        tf_broadcaster_->sendTransform(end_effector_transform_);
    }

private:
    /**
     * @brief Callback function for the set_target_tf service, that updates the transform from the tcp 
     * to the tcp_target frame.
     * 
     * @param request The request from the set_target_tf service.
     * @param response The response from the set_target_tf service.
     */
    void set_target_tf_callback(
        const std::shared_ptr<xarm_interfaces::srv::SetTargetTf::Request> request,
        std::shared_ptr<xarm_interfaces::srv::SetTargetTf::Response> response)
    {
        tcp_transform_.transform.translation = request->target_transform.transform.translation;
        tcp_transform_.transform.rotation = request->target_transform.transform.rotation;
        // call the timer callback to update the transform
        timer_callback();

        response->success = true;
    }
};

/**
 * @brief Main function of the xarm_target_tf_broadcast file that initializes and starts spinning the ROS 2 node.
 *
 * @param argc The number of arguments.
 * @param argv The arguments.
 * @return int The exit code.
 */
int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<XarmTargetTfBroadcaster>());
    rclcpp::shutdown();
    return 0;
}