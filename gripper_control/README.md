# Gripper Control
This package controls the gripper with the Dynamixel motor.

## Content
This package is composed of three nodes defined respectively in the files `gripper_control.cpp` and `gripper_service.cpp` and `gripper_client.cpp`.

* **gripper_control.cpp**:
  > This file contains the node used to control the gripper. It creates a subscriber to the topic `set_position` in order to send a new position goal to the gripper when a new message is received. Additionnally, it creates two services `get_position` and `get_load` that are used to get the current position and load of the gripper.
* **gripper_service.cpp**:
  > This file contains the node used to create the service `set_gripper_order` that is used by the `xarm_main_control` package to control the gripper.
* **gripper_client.cpp**:
  > This file contains the node used to create the clients of the services `get_position` and `get_load` that are used to get the current position and load of the gripper.

## Build Instructions
If you want to build this package, you can use the following command:
```bash
$ colcon build --packages-select gripper_control
```

## Run Instructions
**Note**: Be sure to follow all the requirements, preparation and setup instructions sections of the main [README](../README.md) file before running this package.

To run this package, you can use the following command:
```bash
$ ros2 run gripper_control gripper_control
```
> **Note**: Before running the previous command, you need to make sure that the Dynamixel is connected to the computer and that the port `/dev/ttyUSB0` is available. In particular, make sure that you ran the last command of the [Setup Instructions](../README.md/#setup-instructions) section of the main [README](../README.md) file. The default Dynamixel ID is 10, so make sure that the Dynamixel you are using has this ID too, otherwise you can change it by using the Dynamixel Wizard.

In order to test some services, you can use the following command:
```bash
$ ros2 topic pub -1 /set_position dynamixel_sdk_custom_interfaces/SetPosition "{id: 10, position: 1000}"
$ ros2 service call /get_position dynamixel_sdk_custom_interfaces/srv/GetPosition "id: 10"
$ ros2 service call /get_load xarm_interfaces/srv/GetLoad "id: 10"
```
