#ifndef GRIPPER_CONTROL_HPP_
#define GRIPPER_CONTROL_HPP_

#include <cstdio>
#include <memory>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "rcutils/cmdline_parser.h"
#include "dynamixel_sdk/dynamixel_sdk.h"
#include "dynamixel_sdk_custom_interfaces/msg/set_position.hpp"
#include "dynamixel_sdk_custom_interfaces/srv/get_position.hpp"
#include "xarm_interfaces/srv/get_load.hpp"

/**
 * @brief A class that instantiates a ROS 2 node that communicates with the Dynamixel and
 * allows controlling the gripper.
 *
 */
class GripperControl : public rclcpp::Node
{
public:
  using SetPosition = dynamixel_sdk_custom_interfaces::msg::SetPosition;
  using GetPosition = dynamixel_sdk_custom_interfaces::srv::GetPosition;
  using GetLoad = xarm_interfaces::srv::GetLoad;

  GripperControl();
  virtual ~GripperControl();

private:
  rclcpp::Subscription<SetPosition>::SharedPtr set_position_subscriber_;
  rclcpp::Service<GetPosition>::SharedPtr get_position_service_;
  rclcpp::Service<GetLoad>::SharedPtr get_load_service_;

public:
  static void setupDynamixel(uint8_t dxl_id);
  static void reboot(uint8_t dxl_id);
};

#endif // GRIPPER_CONTROL_HPP_
