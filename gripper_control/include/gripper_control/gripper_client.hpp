#ifndef GRIPPER_CLIENT_HPP_
#define GRIPPER_CLIENT_HPP_

#include "rclcpp/rclcpp.hpp"
#include "dynamixel_sdk_custom_interfaces/msg/set_position.hpp"
#include "dynamixel_sdk_custom_interfaces/srv/get_position.hpp"
#include "xarm_interfaces/srv/get_load.hpp"

using SetPosition = dynamixel_sdk_custom_interfaces::msg::SetPosition;
using GetPosition = dynamixel_sdk_custom_interfaces::srv::GetPosition;
using GetLoad = xarm_interfaces::srv::GetLoad;

using namespace std::chrono_literals;

namespace gripper_control
{
    /**
     * @brief A class that instantiates a ROS 2 node that provides clients to the services used to control the gripper
     * by sending commands to the Dynamixel.
     *
     */
    class GripperClient
    {
    private:
        rclcpp::Node::SharedPtr node_;
        rclcpp::Client<GetPosition>::SharedPtr get_position_client_;
        rclcpp::Client<GetLoad>::SharedPtr get_load_client_;

    public:
        /// @brief The default ID of the Dynamixel used to control the gripper.
        const uint8_t ID_ = 10;

    public:
        GripperClient(const std::string &client_name);

    public:
        int get_position();
        int get_load();
        int get_load_loop(int count = 4);
    };
} // namespace gripper_control

#endif // GRIPPER_CLIENT_HPP_