#include "gripper_control/gripper_client.hpp"

namespace gripper_control
{
    /**
     * @brief Construct a new Gripper Client:: Gripper Client object.
     * 
     * @param client_name The name of the client node.
     */
    GripperClient::GripperClient(const std::string &client_name)
    {
        node_ = rclcpp::Node::make_shared(client_name);
        get_position_client_ = node_->create_client<GetPosition>("get_position");
        get_load_client_ = node_->create_client<GetLoad>("get_load");

        // while (!get_position_client_->wait_for_service(1s) || !get_load_client_->wait_for_service(1s))
        // {
        //     RCLCPP_INFO(node_->get_logger(), "service(s) not available, waiting again...");
        // }

        RCLCPP_INFO(node_->get_logger(), "%s is ready", client_name.c_str());
    }

    /**
     * @brief Client that gets the position of the gripper.
     * 
     * @return int The position of the gripper.
     */
    int GripperClient::get_position()
    {
        auto request = std::make_shared<GetPosition::Request>();
        request->id = ID_;
        auto result_future = get_position_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            return result->position;
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_position");
            return 0;
        }
    }

    /**
     * @brief Client that gets the load of the gripper.
     * 
     * @return int The load of the gripper.
     */
    int GripperClient::get_load()
    {
        auto request = std::make_shared<GetLoad::Request>();
        request->id = ID_;
        auto result_future = get_load_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            return abs(result->load);
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_load");
            return 0;
        }
    }

    /**
     * @brief Get the load of the gripper by looping and sleeping in order to let it stabilize.
     * 
     * @param count The number of times to loop.
     * @return int The load of the gripper.
     */
    int GripperClient::get_load_loop(int count){
        int load = 0;
        for (int i = 0; i < count; i++)
        {
            load = get_load();
            sleep(0.1);
        }
        return load;
    }
}