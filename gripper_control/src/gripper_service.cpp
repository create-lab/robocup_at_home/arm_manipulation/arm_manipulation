#include "rclcpp/rclcpp.hpp"
#include "gripper_control/gripper_client.hpp"
#include "dynamixel_sdk_custom_interfaces/msg/set_position.hpp"
#include "xarm_interfaces/srv/set_gripper_order.hpp"

using SetPosition = dynamixel_sdk_custom_interfaces::msg::SetPosition;
using SetGripperOrder = xarm_interfaces::srv::SetGripperOrder;

/**
 * @brief A class that instantiates a ROS 2 node that provides a service to control the gripper.
 * 
 */
class GripperService : public rclcpp::Node
{
private:
    rclcpp::Publisher<SetPosition>::SharedPtr set_position_publisher_;
    rclcpp::Service<SetGripperOrder>::SharedPtr set_gripper_order_service_;
    gripper_control::GripperClient gripper_client_;
    int max_load_ = 100;

public:
    /**
     * @brief Construct a new Gripper Service object.
     * 
     */
    GripperService() : Node("gripper_service"), gripper_client_("gripper_service_client")
    {
        this->declare_parameter("qos_depth", 10);
        int8_t qos_depth = 0;
        this->get_parameter("qos_depth", qos_depth);

        const auto QOS_RKL10V =
            rclcpp::QoS(rclcpp::KeepLast(qos_depth)).reliable().durability_volatile();

        set_position_publisher_ = this->create_publisher<SetPosition>("set_position", QOS_RKL10V);
        set_gripper_order_service_ = this->create_service<SetGripperOrder>("set_gripper_order", std::bind(&GripperService::set_gripper_order_callback, this, std::placeholders::_1, std::placeholders::_2));

        while (set_position_publisher_->get_subscription_count() == 0)
        {
            RCLCPP_INFO(this->get_logger(), "waiting for subscriber...");
            sleep(1);
        }

        RCLCPP_INFO(this->get_logger(), "gripper_service is ready");
    }

private:
    /**
     * @brief Callback function for the set_gripper_order service.
     * If the order is "open", the gripper will open to the specified position.
     * If the order is "close", the gripper will close until the load reaches the max_load.
     * 
     * @param request The request from the set_gripper_order service.
     * @param response The response from the set_gripper_order service.
     */
    void set_gripper_order_callback(const std::shared_ptr<SetGripperOrder::Request> request, std::shared_ptr<SetGripperOrder::Response> response)
    {
        bool success = false;
        int position = request->position;

        std::string gripper_order = request->order;
        if (gripper_order == "open")
        {
            auto msg = SetPosition();
            while (!all_close(gripper_client_.get_position(), position))
            {   
                msg.position = (int32_t)position;
                msg.id = gripper_client_.ID_;
                set_position_publisher_->publish(msg);
                sleep(1);
            }
            success = true;
        }
        else if (gripper_order == "close")
        {
            auto msg = SetPosition();
            while (gripper_client_.get_load_loop() < max_load_)
            {
                auto load = gripper_client_.get_load_loop();
                auto current_position = gripper_client_.get_position();
                msg.position = (int32_t)current_position - 400 * (max_load_ - load) / max_load_;
                msg.id = gripper_client_.ID_;
                set_position_publisher_->publish(msg);
                sleep(1);
            }
            success = true;
        }
        response->success = success;
    }

private:
    /**
     * @brief Function that checks if the current gripper position is close to the target one.
     * 
     * @param a Current gripper position.
     * @param b Target gripper position.
     * @param tolerance How close the two positions should be at minimum.
     * @return True if the two positions are close enough, false otherwise.
     */
    bool all_close(int a, int b, int tolerance=100)
    {
        return abs(a - b) < tolerance;
    }
};

/**
 * @brief Main function of the gripper_service file that starts spinning the ROS 2 node.
 *
 * @param argc The number of arguments.
 * @param argv The arguments.
 * @return int The exit code.
 */
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<GripperService>());
    rclcpp::shutdown();
    return 0;
}
