#include "gripper_control/gripper_control.hpp"

// Control table address for X series (except XL-320)
#define ADDR_OPERATING_MODE 11
#define ADDR_TORQUE_ENABLE 64
#define ADDR_GOAL_POSITION 116
#define ADDR_PRESENT_POSITION 132
#define ADDR_PRESENT_LOAD 126
#define ADDR_SHUTDOWN 63

// Specifies if reboot is allowed when an error occurs or not
#define REBOOT_ON false

// Protocol version
#define PROTOCOL_VERSION 2.0 // Default Protocol version of DYNAMIXEL X series.

// Default setting
#define BAUDRATE 1000000		   // Default Baudrate of DYNAMIXEL X series
#define DEVICE_NAME "/dev/ttyUSB0" // [Linux]: "/dev/ttyUSB*", [Windows]: "COM*"

dynamixel::PortHandler *portHandler;
dynamixel::PacketHandler *packetHandler;

uint8_t dxl_error = 0;
uint32_t goal_position = 0;
int dxl_comm_result = COMM_TX_FAIL;

/**
 * @brief Construct a new Gripper Control:: Gripper Control object.
 * 
 */
GripperControl::GripperControl()
	: Node("gripper_control")
{
	this->declare_parameter("qos_depth", 10);
	int8_t qos_depth = 0;
	this->get_parameter("qos_depth", qos_depth);

	const auto QOS_RKL10V =
		rclcpp::QoS(rclcpp::KeepLast(qos_depth)).reliable().durability_volatile();

	// create subscriber to listen to set_position topic and send the new position to the Dynamixel
	set_position_subscriber_ =
		this->create_subscription<SetPosition>(
			"set_position",
			QOS_RKL10V,
			[this](const SetPosition::SharedPtr msg) -> void
			{
				uint8_t dxl_error = 0;

				// Enable Dynamixel Torque: useful here when a reboot occurs
				dxl_comm_result = packetHandler->write1ByteTxRx(
					portHandler,
					(uint8_t)msg->id,
					ADDR_TORQUE_ENABLE,
					1,
					&dxl_error);

				// Position Value of X series is 4 byte data.
				// For AX & MX(1.0) use 2 byte data(uint16_t) for the Position Value.
				uint32_t goal_position = (unsigned int)msg->position; // Convert int32 -> uint32
				// Write Goal Position (length : 4 bytes)
				// When writing 2 byte data to AX / MX(1.0), use write2ByteTxRx() instead.
				dxl_comm_result =
					packetHandler->write4ByteTxRx(
						portHandler,
						(uint8_t)msg->id,
						ADDR_GOAL_POSITION,
						goal_position,
						&dxl_error);

				if (dxl_comm_result != COMM_SUCCESS)
				{
					RCLCPP_INFO(this->get_logger(), "%s", packetHandler->getTxRxResult(dxl_comm_result));
				}
				else if (dxl_error != 0)
				{
					RCLCPP_INFO(this->get_logger(), "%s", packetHandler->getRxPacketError(dxl_error));
					// when error occurs, reboot device
					if (REBOOT_ON)
					{
						reboot((uint8_t)msg->id);
					}
				}
			});

	// create service to get the current position of the Dynamixel
	auto get_present_position =
		[this](
			const std::shared_ptr<GetPosition::Request> request,
			std::shared_ptr<GetPosition::Response> response) -> void
	{
		int present_position = 0;
		// Read Present Position (length : 4 bytes) and Convert uint32 -> int32
		// When reading 2 byte data from AX / MX(1.0), use read2ByteTxRx() instead.
		dxl_comm_result = packetHandler->read4ByteTxRx(
			portHandler,
			(uint8_t)request->id,
			ADDR_PRESENT_POSITION,
			reinterpret_cast<uint32_t *>(&present_position),
			&dxl_error);

		response->position = present_position;
	};
	get_position_service_ = create_service<GetPosition>("get_position", get_present_position);

	// create service to get the current load of the Dynamixel
	auto get_present_load =
		[this](
			const std::shared_ptr<GetLoad::Request> request,
			std::shared_ptr<GetLoad::Response> response) -> void
	{
		int present_load = 0;
		dxl_comm_result = packetHandler->read4ByteTxRx(
			portHandler,
			(uint8_t)request->id,
			ADDR_PRESENT_LOAD,
			// this is a hack to convert uint32_t to int32_t
			reinterpret_cast<uint32_t *>(&present_load),
			&dxl_error);

		present_load = (short)present_load;

		response->load = present_load;
	};
	get_load_service_ = create_service<GetLoad>("get_load", get_present_load);

	RCLCPP_INFO(this->get_logger(), "gripper_control is ready.");
}

GripperControl::~GripperControl()
{
}

/**
 * @brief Setup the Dynamixel with the given ID.
 *
 * @param dxl_id The ID of the Dynamixel to setup.
 */
void GripperControl::setupDynamixel(uint8_t dxl_id)
{
	// Use Extended Position Control Mode
	dxl_comm_result = packetHandler->write1ByteTxRx(
		portHandler,
		dxl_id,
		ADDR_OPERATING_MODE,
		4,
		&dxl_error);

	if (dxl_comm_result != COMM_SUCCESS)
	{
		RCLCPP_ERROR(rclcpp::get_logger("gripper_control"), "Failed to set Position Control Mode.");
	}
	else
	{
		RCLCPP_INFO(rclcpp::get_logger("gripper_control"), "Succeeded to set Position Control Mode.");
	}

	// Enable Torque of DYNAMIXEL
	dxl_comm_result = packetHandler->write1ByteTxRx(
		portHandler,
		dxl_id,
		ADDR_TORQUE_ENABLE,
		1,
		&dxl_error);

	if (dxl_comm_result != COMM_SUCCESS)
	{
		RCLCPP_ERROR(rclcpp::get_logger("gripper_control"), "Failed to enable torque.");
	}
	else
	{
		RCLCPP_INFO(rclcpp::get_logger("gripper_control"), "Succeeded to enable torque.");
	}

	// Disable shutdown of DYNAMIXEL, usefull to prevent from having to reboot the device
	if (REBOOT_ON)
	{
		dxl_comm_result = packetHandler->write1ByteTxRx(
			portHandler,
			dxl_id,
			ADDR_SHUTDOWN,
			0,
			&dxl_error);

		if (dxl_comm_result != COMM_SUCCESS)
		{
			RCLCPP_ERROR(rclcpp::get_logger("gripper_control"), "Failed to disable shutdown.");
		}
		else
		{
			RCLCPP_INFO(rclcpp::get_logger("gripper_control"), "Succeeded to disable shutdown.");
		}
	}
}

/**
 * @brief Reboot the Dynamixel with the given ID.
 *
 * @param dxl_id The ID of the Dynamixel to reboot.
 */
void GripperControl::reboot(uint8_t dxl_id)
{
	// Reboot DYNAMIXEL
	dxl_comm_result = packetHandler->reboot(
		portHandler,
		dxl_id,
		&dxl_error);

	if (dxl_comm_result != COMM_SUCCESS)
	{
		RCLCPP_ERROR(rclcpp::get_logger("gripper_control"), "Failed to reboot.");
	}
	else
	{
		RCLCPP_INFO(rclcpp::get_logger("gripper_control"), "Succeeded to reboot.");
	}
}

/**
 * @brief Main function of the gripper_control file that initializes the Dynamixel and starts spinning the ROS 2 node.
 *
 * @param argc The number of arguments.
 * @param argv The arguments.
 * @return int The exit code.
 */
int main(int argc, char *argv[])
{
	portHandler = dynamixel::PortHandler::getPortHandler(DEVICE_NAME);
	packetHandler = dynamixel::PacketHandler::getPacketHandler(PROTOCOL_VERSION);

	// Open Serial Port
	dxl_comm_result = portHandler->openPort();
	if (dxl_comm_result == false)
	{
		RCLCPP_ERROR(rclcpp::get_logger("gripper_control"), "Failed to open the port!");
		return -1;
	}
	else
	{
		RCLCPP_INFO(rclcpp::get_logger("gripper_control"), "Succeeded to open the port.");
	}

	// Set the baudrate of the serial port (use DYNAMIXEL Baudrate)
	dxl_comm_result = portHandler->setBaudRate(BAUDRATE);
	if (dxl_comm_result == false)
	{
		RCLCPP_ERROR(rclcpp::get_logger("gripper_control"), "Failed to set the baudrate!");
		return -1;
	}
	else
	{
		RCLCPP_INFO(rclcpp::get_logger("gripper_control"), "Succeeded to set the baudrate.");
	}

	// Setup all Dynamixels connected to the bus thanks to BROADCAST_ID(254)
	GripperControl::setupDynamixel(BROADCAST_ID);

	rclcpp::init(argc, argv);

	auto gripper_control = std::make_shared<GripperControl>();
	rclcpp::spin(gripper_control);
	rclcpp::shutdown();

	// Disable Torque of all connected Dynamixels
	packetHandler->write1ByteTxRx(
		portHandler,
		BROADCAST_ID,
		ADDR_TORQUE_ENABLE,
		0,
		&dxl_error);

	return 0;
}
