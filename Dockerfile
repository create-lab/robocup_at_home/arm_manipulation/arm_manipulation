###########################################
# Arm Manipulation image
###########################################
FROM hmirandaqueiros/arm_manipulation:base

ENV DEBIAN_FRONTEND=noninteractive

# Download the Detectron2 weights
WORKDIR /home/$USERNAME/dev_ws/src/ressource
RUN wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1z9I7aZmqr9qiw2fKSLJYLcbKJiMXylN_' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1z9I7aZmqr9qiw2fKSLJYLcbKJiMXylN_" -O model_best_train_output_batch4_epoch50_lr0.001_202303310127.pth && rm -rf /tmp/cookies.txt

# Allow non-root user to access the USB ports
RUN usermod -aG dialout $USERNAME

# Allow non-root user to use ZED Mini camera
RUN usermod -aG video $USERNAME

# Set a diretory to store the project
WORKDIR /home/$USERNAME/dev_ws/src/arm_manipulation
COPY . .

# Set a directory to build the project
WORKDIR /home/$USERNAME/dev_ws

# Install all the ROS 2 dependencies
RUN apt-get update && apt-get upgrade -y \
    && apt-get install -y ros-core
RUN rosdep update \
    && rosdep install -i --from-path src --rosdistro humble -y

# Build the project
RUN /bin/bash -c "source /opt/ros/${ROS_DISTRO}/setup.sh && colcon build"
# Add the source of the project to the .bashrc
RUN echo "if [ -f /home/$USERNAME/dev_ws/install/setup.bash ]; then source /home/$USERNAME/dev_ws/install/setup.bash; fi" >> /home/$USERNAME/.bashrc

# Change to the non-root user in order to be able to run UI applications
USER $USERNAME

ENV DEBIAN_FRONTEND=

ENTRYPOINT /bin/bash -c "sudo colcon build && /bin/bash"