# Ensures that all the basic commands like pull, checkout, etc. take into account all the submodules recursively
git config submodule.recurse true
# Ensures that at every push, the submodules changes are pushed as well
git config push.recurseSubmodules on-demand
# Updates the submodules URL changes
git submodule sync --recursive
# Updates the submodules to the latest commit recursively by applying the merge method
git submodule update --recursive --init --remote --merge
# The following commands update the branches tracking the upstream branches
cd ./xarm_ros2/xarm_sdk/cxx
git checkout master
git pull upstream master
git push origin master
git checkout custom
cd ../..
git checkout humble
git pull upstream humble
git push origin humble
git checkout custom_humble
cd ..
